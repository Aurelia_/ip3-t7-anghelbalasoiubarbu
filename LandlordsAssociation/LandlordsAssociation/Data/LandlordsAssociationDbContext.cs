﻿using LandlordsAssociation.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LandlordsAssociation.Data
{
    public class LandlordsAssociationDbContext : DbContext
    {
        public LandlordsAssociationDbContext(DbContextOptions<LandlordsAssociationDbContext> options)
        : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // the administrator's user account
            modelBuilder.Entity<Users>().HasData(
                new Users
                {
                    Id = 1,
                    Username = "la_admin@la.com",
                    Password = "LandlordsAssociation@la",
                    Role = "Administrator"
                }
                ) ;

            modelBuilder.Entity<Employees>().HasData(
                new Employees
                {
                    CNP = "1212122343445",
                    FirstName = "L",
                    LastName = "A",
                    Salary = 0,
                    Address = "-",
                    Email = "la_admin@la.com",
                    PhoneNumber = "-",
                    Job = "Secretary",
                    UserId = 1
                }
                );

            modelBuilder.Entity<BlockOfFlats>().HasData(
                new BlockOfFlats 
                {
                    Id = 1,
                    Street = "Petre Ispirescu",
                    City = "Craiova"
                },
                new BlockOfFlats 
                {
                    Id = 2,
                    Street = "Petre Ispirescu",
                    City = "Craiova"
                },
                new BlockOfFlats 
                {
                    Id = 3,
                    Street = "Calea Bucuresti",
                    City = "Craiova"
                }
            );
            modelBuilder.Entity<Apartments>().HasData(
                new Apartments
                {
                    Id = 1,
                    NumberRooms = 3,
                    NumberPersons = 0,
                    Floor = 0,
                    Entrance = "A",
                    BlockId = 1,
                    ContractId = null,
                    Number = 1
                },
                new Apartments
                {
                    Id = 2,
                    NumberRooms = 3,
                    NumberPersons = 0,
                    Floor = 0,
                    Entrance = "A",
                    BlockId = 1,
                    ContractId = null,
                    Number = 2
                },
                new Apartments
                {
                    Id = 3,
                    NumberRooms = 3,
                    NumberPersons = 0,
                    Floor = 0,
                    Entrance = "A",
                    BlockId = 1,
                    ContractId = null,
                    Number = 3
                },
                new Apartments
                {
                    Id = 4,
                    NumberRooms = 3,
                    NumberPersons = 0,
                    Floor = 0,
                    Entrance = "A",
                    BlockId = 1,
                    ContractId = null,
                    Number = 4
                },
                new Apartments
                {
                    Id = 5,
                    NumberRooms = 3,
                    NumberPersons = 0,
                    Floor = 1,
                    Entrance = "A",
                    BlockId = 1,
                    ContractId = null,
                    Number = 5
                },
                new Apartments
                {
                    Id = 6,
                    NumberRooms = 3,
                    NumberPersons = 0,
                    Floor = 1,
                    Entrance = "A",
                    BlockId = 1,
                    ContractId = null,
                    Number = 6
                },
                new Apartments
                {
                    Id = 7,
                    NumberRooms = 3,
                    NumberPersons = 0,
                    Floor = 1,
                    Entrance = "A",
                    BlockId = 1,
                    ContractId = null,
                    Number = 7
                },
                new Apartments
                {
                    Id = 8,
                    NumberRooms = 3,
                    NumberPersons = 0,
                    Floor = 1,
                    Entrance = "A",
                    BlockId = 1,
                    ContractId = null,
                    Number = 8
                },
                new Apartments
                {
                    Id = 9,
                    NumberRooms = 2,
                    NumberPersons = 0,
                    Floor = 0,
                    Entrance = "B",
                    BlockId = 1,
                    ContractId = null,
                    Number = 1
                }, new Apartments
                {
                    Id = 10,
                    NumberRooms = 2,
                    NumberPersons = 0,
                    Floor = 0,
                    Entrance = "B",
                    BlockId = 1,
                    ContractId = null,
                    Number = 2
                }, new Apartments
                {
                    Id = 11,
                    NumberRooms = 2,
                    NumberPersons = 0,
                    Floor = 0,
                    Entrance = "B",
                    BlockId = 1,
                    ContractId = null,
                    Number = 3
                }, new Apartments
                {
                    Id = 12,
                    NumberRooms = 2,
                    NumberPersons = 0,
                    Floor = 0,
                    Entrance = "B",
                    BlockId = 1,
                    ContractId = null,
                    Number = 4
                }, new Apartments
                {
                    Id = 13,
                    NumberRooms = 2,
                    NumberPersons = 0,
                    Floor = 1,
                    Entrance = "B",
                    BlockId = 1,
                    ContractId = null,
                    Number = 5
                }, new Apartments
                {
                    Id = 14,
                    NumberRooms = 2,
                    NumberPersons = 0,
                    Floor = 1,
                    Entrance = "B",
                    BlockId = 1,
                    ContractId = null,
                    Number = 6
                }, new Apartments
                {
                    Id = 15,
                    NumberRooms = 2,
                    NumberPersons = 0,
                    Floor = 1,
                    Entrance = "B",
                    BlockId = 1,
                    ContractId = null,
                    Number = 7
                }, new Apartments
                {
                    Id = 16,
                    NumberRooms = 2,
                    NumberPersons = 0,
                    Floor = 1,
                    Entrance = "B",
                    BlockId = 1,
                    ContractId = null,
                    Number = 8
                }, 
                new Apartments
                {
                    Id = 17,
                    NumberRooms = 1,
                    NumberPersons = 0,
                    Floor = 0,
                    Entrance = "A",
                    BlockId = 2,
                    ContractId = null,
                    Number = 1
                }, 
                new Apartments
                {
                    Id = 18,
                    NumberRooms = 1,
                    NumberPersons = 0,
                    Floor = 0,
                    Entrance = "A",
                    BlockId = 2,
                    ContractId = null,
                    Number = 2
                }, 
                new Apartments
                {
                    Id = 19,
                    NumberRooms = 1,
                    NumberPersons = 0,
                    Floor = 0,
                    Entrance = "A",
                    BlockId = 2,
                    ContractId = null,
                    Number = 3
                },
                new Apartments
                {
                    Id = 20,
                    NumberRooms = 1,
                    NumberPersons = 0,
                    Floor = 0,
                    Entrance = "A",
                    BlockId = 2,
                    ContractId = null,
                    Number = 4
                },
                new Apartments
                {
                    Id = 21,
                    NumberRooms = 1,
                    NumberPersons = 0,
                    Floor = 1,
                    Entrance = "A",
                    BlockId = 2,
                    ContractId = null,
                    Number = 5
                },
                new Apartments
                {
                    Id = 22,
                    NumberRooms = 1,
                    NumberPersons = 0,
                    Floor = 1,
                    Entrance = "A",
                    BlockId = 2,
                    ContractId = null,
                    Number = 6
                },
                new Apartments
                {
                    Id = 23,
                    NumberRooms = 1,
                    NumberPersons = 0,
                    Floor = 1,
                    Entrance = "A",
                    BlockId = 2,
                    ContractId = null,
                    Number = 7
                },
                new Apartments
                {
                    Id = 24,
                    NumberRooms = 1,
                    NumberPersons = 0,
                    Floor = 1,
                    Entrance = "A",
                    BlockId = 2,
                    ContractId = null,
                    Number = 8
                },
                new Apartments
                {
                    Id = 25,
                    NumberRooms = 2,
                    NumberPersons = 0,
                    Floor = 0,
                    Entrance = "B",
                    BlockId = 2,
                    ContractId = null,
                    Number = 1
                },
                new Apartments
                {
                    Id = 26,
                    NumberRooms = 2,
                    NumberPersons = 0,
                    Floor = 0,
                    Entrance = "B",
                    BlockId = 2,
                    ContractId = null,
                    Number = 2
                },
                new Apartments
                {
                    Id = 27,
                    NumberRooms = 2,
                    NumberPersons = 0,
                    Floor = 0,
                    Entrance = "B",
                    BlockId = 2,
                    ContractId = null,
                    Number = 3
                },
                new Apartments
                {
                    Id = 28,
                    NumberRooms = 2,
                    NumberPersons = 0,
                    Floor = 0,
                    Entrance = "B",
                    BlockId = 2,
                    ContractId = null,
                    Number = 4
                },
                new Apartments
                {
                    Id = 29,
                    NumberRooms = 2,
                    NumberPersons = 0,
                    Floor = 1,
                    Entrance = "B",
                    BlockId = 2,
                    ContractId = null,
                    Number = 5
                },
                new Apartments
                {
                    Id = 30,
                    NumberRooms = 2,
                    NumberPersons = 0,
                    Floor = 1,
                    Entrance = "B",
                    BlockId = 2,
                    ContractId = null,
                    Number = 6
                },
                new Apartments
                {
                    Id = 31,
                    NumberRooms = 2,
                    NumberPersons = 0,
                    Floor = 1,
                    Entrance = "B",
                    BlockId = 2,
                    ContractId = null,
                    Number = 7
                },
                new Apartments
                {
                    Id = 32,
                    NumberRooms = 2,
                    NumberPersons = 0,
                    Floor = 1,
                    Entrance = "B",
                    BlockId = 2,
                    ContractId = null,
                    Number = 8
                },
                new Apartments
                {
                    Id = 33,
                    NumberRooms = 4,
                    NumberPersons = 0,
                    Floor = 0,
                    Entrance = "A",
                    BlockId = 3,
                    ContractId = null,
                    Number = 1
                },
                new Apartments
                {
                    Id = 34,
                    NumberRooms = 4,
                    NumberPersons = 0,
                    Floor = 0,
                    Entrance = "A",
                    BlockId = 3,
                    ContractId = null,
                    Number = 2
                },
                new Apartments
                {
                    Id = 35,
                    NumberRooms = 4,
                    NumberPersons = 0,
                    Floor = 0,
                    Entrance = "A",
                    BlockId = 3,
                    ContractId = null,
                    Number = 3
                },
                new Apartments
                {
                    Id = 36,
                    NumberRooms = 4,
                    NumberPersons = 0,
                    Floor = 1,
                    Entrance = "A",
                    BlockId = 3,
                    ContractId = null,
                    Number = 4
                },
                new Apartments
                {
                    Id = 37,
                    NumberRooms = 4,
                    NumberPersons = 0,
                    Floor = 1,
                    Entrance = "A",
                    BlockId = 3,
                    ContractId = null,
                    Number = 5
                },
                new Apartments
                {
                    Id = 38,
                    NumberRooms = 4,
                    NumberPersons = 0,
                    Floor = 1,
                    Entrance = "A",
                    BlockId = 3,
                    ContractId = null,
                    Number = 6
                },
                new Apartments
                {
                    Id = 39,
                    NumberRooms = 4,
                    NumberPersons = 0,
                    Floor = 0,
                    Entrance = "B",
                    BlockId = 3,
                    ContractId = null,
                    Number = 1
                },
                new Apartments
                {
                    Id = 40,
                    NumberRooms = 4,
                    NumberPersons = 0,
                    Floor = 0,
                    Entrance = "B",
                    BlockId = 3,
                    ContractId = null,
                    Number = 2
                },
                new Apartments
                {
                    Id = 41,
                    NumberRooms = 4,
                    NumberPersons = 0,
                    Floor = 0,
                    Entrance = "B",
                    BlockId = 3,
                    ContractId = null,
                    Number = 3
                },
                new Apartments
                {
                    Id = 42,
                    NumberRooms = 4,
                    NumberPersons = 0,
                    Floor = 1,
                    Entrance = "B",
                    BlockId = 3,
                    ContractId = null,
                    Number = 4
                },
                new Apartments
                {
                    Id = 43,
                    NumberRooms = 4,
                    NumberPersons = 0,
                    Floor = 1,
                    Entrance = "B",
                    BlockId = 3,
                    ContractId = null,
                    Number = 5
                },
                new Apartments
                {
                    Id = 44,
                    NumberRooms = 4,
                    NumberPersons = 0,
                    Floor = 1,
                    Entrance = "B",
                    BlockId = 3,
                    ContractId = null,
                    Number = 6
                }
                );
            
        }

        public DbSet<AdditionalBills> AdditionalBills { get; set; }
        public DbSet<Apartments> Apartments { get; set; }
        public DbSet<BlockOfFlats> BlockOfFlats { get; set; }
        public DbSet<Employees> Employees { get; set; }
        public DbSet<ProviderBills> ProviderBills { get; set; }
        public DbSet<ProviderContracts> ProviderContracts { get; set; }
        public DbSet<Providers> Providers { get; set; }
        public DbSet<TenantContracts> TenantContracts { get; set; }
        public DbSet<Tenants> Tenants { get; set; }
        public DbSet<WaterConsumption> WaterConsumption { get; set; }
        public DbSet<Rent> Rent { get; set; }
        public DbSet<Users> Users { get; set; }
        public DbSet<Payment> Payments { get; set; }
    }
}
