﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LandlordsAssociation.Migrations
{
    public partial class UpdateKeys : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
               name: "FK_Apartments_TenantContracts_ContractId",
               table: "Apartments");

            migrationBuilder.AddForeignKey(
                name: "FK_Apartments_TenantContracts_ContractId",
                table: "Apartments",
                column: "ContractId",
                principalTable: "TenantContracts",
                schema: null,
                principalSchema: null,
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull
                );

            migrationBuilder.DropForeignKey(
                name: "FK_TenantContracts_Tenants_TenantCNP",
                table: "TenantContracts"
                );

            migrationBuilder.AddForeignKey(
                name: "FK_TenantContracts_Tenants_TenantCNP",
                table: "TenantContracts",
                column: "TenantCNP",
                principalTable: "Tenants",
                schema: null,
                principalSchema: null,
                principalColumn: "CNP",
                onDelete: ReferentialAction.Cascade
                );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
              name: "FK_Apartments_TenantContracts_ContractId",
              table: "Apartments");

            migrationBuilder.AddForeignKey(
                name: "FK_Apartments_TenantContracts_ContractId",
                table: "Apartments",
                column: "ContractId",
                principalTable: "TenantContracts",
                schema: null,
                principalSchema: null,
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction
                );

            migrationBuilder.DropForeignKey(
                name: "FK_TenantContracts_Tenants_TenantCNP",
                table: "TenantContracts"
                );

            migrationBuilder.AddForeignKey(
                name: "FK_TenantContracts_Tenants_TenantCNP",
                table: "TenantContracts",
                column: "TenantCNP",
                principalTable: "Tenants",
                schema: null,
                principalSchema: null,
                principalColumn: "CNP",
                onDelete: ReferentialAction.NoAction
                );
        }
    }
}
