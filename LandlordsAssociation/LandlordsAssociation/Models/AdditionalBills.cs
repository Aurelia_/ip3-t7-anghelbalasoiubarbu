﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LandlordsAssociation.Models
{
    public class AdditionalBills
    {
        [Key]
        public int Id { get; set; }
        public string Service { get; set; }
        [Range(1, 100000)]
        public float Value { get; set; }
        public DateTime Date { get; set; }
    }
}
