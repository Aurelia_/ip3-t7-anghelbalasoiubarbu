﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LandlordsAssociation.Models
{
    public class Apartments
    {
        [Key]
        public int Id { get; set; }
        public int Number { get; set; }
        public int NumberRooms { get; set; }
        public int NumberPersons { get; set; }
        public int Floor { get; set; }
        public string Entrance { get; set; }

        [ForeignKey("BlockOfFlats")]
        public int BlockId { get; set; }

        [ForeignKey("TenantContracts")]
        public int? ContractId { get; set; }

        // Navigation properties
        public BlockOfFlats Block { get; set; }
        public TenantContracts Contract { get; set; }
        public ICollection<WaterConsumption> WaterConsumptions { get; set; }
        public ICollection<ProviderBills> ProviderBills { get; set; }
        public ICollection<Rent> Rents{ get; set; }
        public ICollection<Payment> Payments { get; set; }
    }
}
