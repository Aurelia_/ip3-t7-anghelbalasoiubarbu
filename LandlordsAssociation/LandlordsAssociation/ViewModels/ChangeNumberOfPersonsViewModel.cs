﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LandlordsAssociation.ViewModels
{
    public class ChangeNumberOfPersonsViewModel
    {
        [Required]
        [Range(1, 100, ErrorMessage = "The id of the block must be in the interval [1, 100]")]
        public int BlockId { get; set; }
        [Required]
        public string Entrance { get; set; }
        [Required]
        [Range(1,30, ErrorMessage = "The number of the apartment must be in the interval [1, 30]")]
        public int ApartmentNumber { get; set; }
        [Required]
        [Range(0,10, ErrorMessage = "The number of persons must be in the interval [0, 10]")]
        public int NumberOfPersons { get; set; }

    }
}
