﻿using LandlordsAssociation.Data;
using LandlordsAssociation.Models;
using LandlordsAssociation.ViewModels;
using System;
using System.Linq;
using Xunit;

namespace LandlordsAssociation.Tests
{
    public class ApartmentsControllerTest
    {
        private LandlordsAssociationDbContext context;
        public ApartmentsControllerTest()
        {
            var factory = new ConnectionFactory();
            context = factory.CreateContextForInMemory();
            Seed(context);
        }

        [Fact]
        public void AssignApartmentToTenant_ExistingTenantAndApartment_Succeeds()
        {
            string tenantCNP = "2751090786543";
            int noPersons = 1;
            string contractType = "Rent";
            float value = 1500;
            int apartmentNumber = 1;
            int blockNumber = 1;
            string entrance = "A";

            Apartments apartment = context.Apartments.Where(a => (a.Number == apartmentNumber) && (a.BlockId == blockNumber)
               && (a.Entrance == entrance) && (a.ContractId == null)).FirstOrDefault();
            if (apartment != null)
            {
                TenantContracts contract = new TenantContracts
                {
                    Type = contractType,
                    Value = value,
                    Date = DateTime.Today,
                    TenantCNP = tenantCNP
                };
                context.TenantContracts.Add(contract);
                apartment.ContractId = contract.Id;
                apartment.NumberPersons = noPersons;
                context.Apartments.Update(apartment);
                context.SaveChanges();
            }
            Assert.NotNull(apartment.ContractId);
            Assert.Equal(1, apartment.NumberPersons);
        }

        [Fact]
        public void UpdatePersonsNumber_Number_Succeeds()
        {
            int apartmentNumber = 1;
            int blockNumber = 12;
            string entrance = "A";
            var apartment = context.Apartments.Where(a => a.Number == apartmentNumber && a.BlockId == blockNumber
                          && String.Equals(a.Entrance, entrance)).FirstOrDefault();
            apartment.NumberPersons = 2;
            context.Apartments.Update(apartment);
            context.SaveChanges();
            Assert.Equal(2, apartment.NumberPersons);   
        }


        private void Seed(LandlordsAssociationDbContext context)
        {
            var block = new BlockOfFlats
            {
                Id = 12,
                City = "Craiova",
                Street = "Mihai Viteazul"
            };
            context.BlockOfFlats.Add(block);
            var apartment = new Apartments
            {
                Id = 50,
                BlockId = 12,
                ContractId = null,
                Entrance = "A",
                Floor = 1,
                Number = 1,
                NumberRooms = 4,
                NumberPersons = 0
            };
            context.Apartments.Add(apartment);
            var tenant = new Tenants
            {
                CNP = "2751090786543",
                FirstName = "Robin",
                LastName = "Margineanu",
                PhoneNumber = "0767876543"
            };
            context.Tenants.Add(tenant);
            context.SaveChanges();
        }
    }
}
