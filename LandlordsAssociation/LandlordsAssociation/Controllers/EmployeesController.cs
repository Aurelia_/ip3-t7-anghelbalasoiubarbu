﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using LandlordsAssociation.Data;
using LandlordsAssociation.Models;
using LandlordsAssociation.Services.Interfaces;
using LandlordsAssociation.ViewModels;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;

namespace LandlordsAssociation.Controllers
{
    public class EmployeesController : Controller
    {
        private readonly LandlordsAssociationDbContext _context;
        private IPasswordGenerator _passwordService;

        public EmployeesController(LandlordsAssociationDbContext context, IPasswordGenerator passwordService)
        {
            _context = context;
            _passwordService = passwordService;
        }

        // GET: Employees/Create
        public IActionResult Create()
        {
            var sessionContent = HttpContext.Session.GetString("loggedInUser");
            // check if a user is logged
            if (sessionContent == null)
            {
                return RedirectToAction("Index", "Home");
            }
            //get the details of the logged in user
            var user = JsonConvert.DeserializeObject<Users>(sessionContent);
            int id = user.Id;
            var foundUser = _context.Users.Where(u => u.Id == id).Single();

            if (foundUser.Role.Equals("Administrator"))
            {
                return View();
            }
            else
            {
                return RedirectToAction("Logout", "Users");
            }
        }

        // POST: Employees/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("CNP,FirstName,LastName,Salary,Address,Email,PhoneNumber,Job")] Employees employees)
        {
            var foundEmployee = _context.Employees.Where(e => e.CNP == employees.CNP).SingleOrDefault();
            if (ModelState.IsValid && foundEmployee == null)
            {
                if(employees.Job.Equals("Secretary") || employees.Job.Equals("Accountant") || employees.Job.Equals("Block administrator"))
                {
                    Users user = new Users
                    {
                        Username = employees.Email,
                        Password = _passwordService.GeneratePassword(10)
                    };
                    if(employees.Job.Equals("Block administrator"))
                    {
                        user.Role = "User";
                    }
                    else
                    {
                        user.Role = "Administrator";
                    }
                    _context.Add(user);
                    _context.SaveChanges();
                    employees.UserId = user.Id;
                }
                else
                {
                    employees.User = null;
                    employees.UserId = null;
                }
                
                
                _context.Add(employees);
                _context.SaveChanges();
                
                if(employees.User != null)
                {
                    return RedirectToAction("Details", "Users", new { id = employees.UserId });
                }
                else
                {
                    return RedirectToAction("SeeIncomeAndCosts", "Bills");
                }
                
            }
            ViewData["Message"] = "The employee already exists or the data is invalid...";
            return View();
        }

        // GET: Employees/Edit/5
        public async Task<IActionResult> Edit()
        {
            var sessionContent = HttpContext.Session.GetString("loggedInUser");
            // check if a user is logged
            if (sessionContent == null)
            {
                return RedirectToAction("Index", "Home");
            }
            //get the details of the logged in user
            var user = JsonConvert.DeserializeObject<Users>(sessionContent);
            int id = user.Id;
            var foundUser = _context.Users.Where(u => u.Id == id).Single();

            if (foundUser.Role.Equals("Administrator"))
            {
                ViewData["Employees"] = new SelectList(_context.Employees.ToList(), "CNP", "CNP");

                return View();
            }
            else
            {
                return RedirectToAction("Logout", "Users");
            }
        }

        // POST: Employees/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("CNP,FirstName,LastName,Salary,Address,Email,PhoneNumber,Job")] Employees employees)
        {
            int userId = 0;
            if (ModelState.IsValid)
            {
                try
                {
                    var foundEmployee = _context.Employees.Where(e => e.CNP.Equals(employees.CNP)).Include(e => e.User).SingleOrDefault();
                    
                    if (employees.Email != foundEmployee.Email)
                    {
                        // daca s-a schimbat email-ul
                        var foundUser = _context.Users.Where(u => u.Id == foundEmployee.UserId).SingleOrDefault();
                        if(foundUser != null)
                        {
                            // daca angajatul are cont
                            foundUser.Username = employees.Email;
                            _context.Update(foundUser);
                            _context.SaveChanges();
                        }
                        
                    }
                    // actualizeaza datele
                    foundEmployee.LastName = employees.LastName;
                    foundEmployee.FirstName = employees.FirstName;
                    foundEmployee.Address = employees.Address;
                    foundEmployee.Email = employees.Email;
                    foundEmployee.Job = employees.Job;
                    foundEmployee.PhoneNumber = employees.PhoneNumber;
                    foundEmployee.Salary = employees.Salary;
                    
                    if (foundEmployee.Job.Equals("Secretary") || foundEmployee.Job.Equals("Accountant") || foundEmployee.Job.Equals("Block administrator"))
                    {
                       
                        if(foundEmployee.UserId == null)
                        {
                            // daca angajatul si-a schimbat job-ul si are nevoie de cont
                            Users user = new Users
                            {
                                Username = employees.Email,
                                Password = _passwordService.GeneratePassword(10)
                            };
                            if (employees.Job.Equals("Block administrator"))
                            {
                                user.Role = "User";
                            }
                            else
                            {
                                user.Role = "Administrator";
                            }
                            _context.Add(user);
                            _context.SaveChanges();
                            foundEmployee.UserId = user.Id;
                            userId = user.Id;
                        }
                        else
                        {
                            var foundUser = _context.Users.Where(u => u.Id == foundEmployee.UserId).SingleOrDefault();
                            if(foundEmployee.Job.Equals("Secretary") || foundEmployee.Job.Equals("Accountant") && foundUser.Role.Equals("User"))
                            {
                                foundUser.Role = "Administrator";
                                _context.Update(foundUser);
                                _context.SaveChanges();
                            }
                            if (foundEmployee.Job.Equals("Block administrator") && foundUser.Role.Equals("Administrator"))
                            {
                                foundUser.Role = "User";
                                _context.Update(foundUser);
                                _context.SaveChanges();
                            }
                        }

                    }
                    else
                    {
                        // daca angajatul si-a schimbat job-ul si nu are nevoie de cont
                        if(foundEmployee.UserId != null)
                        {
                            var redundantUser = _context.Users.Where(u => u.Id == foundEmployee.UserId).SingleOrDefault();
                            foundEmployee.UserId = null;
                            foundEmployee.User = null;
                            _context.Users.Remove(redundantUser);
                            _context.SaveChanges();
                        }
                    }
                    // update:
                    _context.Update(foundEmployee);
                    _context.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!_context.Employees.Any(e => e.CNP == employees.CNP))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                if (userId != 0)
                {
                    return RedirectToAction("Details", "Users", new { id = userId });
                }
                else
                {
                    return RedirectToAction("SeeIncomeAndCosts", "Bills");
                }
                
            }
            return View();
        }

        public IActionResult GetEmployeeByCNP(string cnp)
        {
            return Json(_context.Employees.Where(e => e.CNP.Equals(cnp)).SingleOrDefault());
        }

        [HttpGet]
        public IActionResult Delete()
        {
            var sessionContent = HttpContext.Session.GetString("loggedInUser");
            // check if a user is logged
            if (sessionContent == null)
            {
                return RedirectToAction("Index", "Home");
            }
            //get the details of the logged in user
            var user = JsonConvert.DeserializeObject<Users>(sessionContent);
            int id = user.Id;
            var foundUser = _context.Users.Where(u => u.Id == id).Single();

            if (foundUser.Role.Equals("Administrator"))
            {
                ViewData["FirstNames"] = new SelectList(_context.Employees.ToList(), "FirstName", "FirstName");
                DeleteEmployeeViewModel model = new DeleteEmployeeViewModel();
                return View(model);
            }
            else
            {
                return RedirectToAction("Logout", "Users");
            }
        }

        public IActionResult GetLastName(string id)
        {
            return Json(_context.Employees.Where(e => e.FirstName.Equals(id)).ToList().OrderBy(e => e.FirstName).Distinct());
        }

        public IActionResult GetCNP(string firstName, string lastName)
        {
            return Json(_context.Employees.Where(e => e.FirstName.Equals(firstName)).Where(e => e.LastName.Equals(lastName)).ToList());
        }

        // GET: Employees/Delete
        [HttpPost, ActionName("Delete")]
        public async Task<IActionResult> Delete(DeleteEmployeeViewModel model)
        {
            if (model.CNP == null)
            {
                return RedirectToAction("Delete", "Employees");
            }

            
            var employees = await _context.Employees
                .Include(e => e.User)
                .FirstOrDefaultAsync(m => m.CNP == model.CNP);
           
            
            if (employees == null)
            {
                return RedirectToAction("Delete", "Employees");
            }
            else
            {
                var foundUser = _context.Users.Where(u => u.Id == employees.UserId).SingleOrDefault();
                if (foundUser != null)
                {
                    _context.Users.Remove(foundUser);
                }
                _context.Employees.Remove(employees);
                _context.SaveChanges();
                return RedirectToAction("SeeIncomeAndCosts", "Bills");
            }
        }
    }
}
