﻿using LandlordsAssociation.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LandlordsAssociation.ViewModels
{
    public class AddProviderBillViewModel
    {
        public string ProviderName { get; set; }
        public int Id { get; set; }
        [Range(0, float.MaxValue, ErrorMessage ="The bill's value must be positive.")]
        public float Value { get; set; }
        public string TenantCNP { get; set; }
        public int BlockNumber { get; set; }
        public int ApartmentNumber { get; set; }
        public string Entrance { get; set; }
    }
}
