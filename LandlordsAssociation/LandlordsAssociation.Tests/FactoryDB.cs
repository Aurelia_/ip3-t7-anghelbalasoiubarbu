﻿using LandlordsAssociation.Data;
using Microsoft.EntityFrameworkCore;

namespace LandlordsAssociation.Tests
{
    public class FactoryDB
    {
        public static LandlordsAssociationDbContext CreateDB()
        {
            var options = new DbContextOptionsBuilder<LandlordsAssociationDbContext>()
             .UseInMemoryDatabase(databaseName: "LandlordsAssociationTestDatabase")
             .Options;
            return new LandlordsAssociationDbContext(options);
        }
    }
}
