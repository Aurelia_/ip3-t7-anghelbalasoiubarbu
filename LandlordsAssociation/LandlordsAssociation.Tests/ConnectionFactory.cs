﻿using LandlordsAssociation.Data;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using System;

namespace LandlordsAssociation.Tests
{
    public class ConnectionFactory : IDisposable
    {
        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        public LandlordsAssociationDbContext CreateContextForInMemory()
        {
            var option = new DbContextOptionsBuilder<LandlordsAssociationDbContext>().UseInMemoryDatabase(databaseName: "Test_Database").Options;

            var context = new LandlordsAssociationDbContext(option);
            if (context != null)
            {
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();
            }
            return context;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}
