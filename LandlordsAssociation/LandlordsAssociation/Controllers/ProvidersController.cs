﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using LandlordsAssociation.Data;
using LandlordsAssociation.Models;
using LandlordsAssociation.ViewModels;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;

namespace LandlordsAssociation.Controllers
{
    public class ProvidersController : Controller
    {
        private readonly LandlordsAssociationDbContext _context;

        public ProvidersController(LandlordsAssociationDbContext context)
        {
            _context = context;
        }

        public IActionResult Create()
        {
            var sessionContent = HttpContext.Session.GetString("loggedInUser");
            if (sessionContent == null)
            {
                return RedirectToAction("Index", "Home");
            }
            var user = JsonConvert.DeserializeObject<Users>(sessionContent);
            int id = user.Id;
            var foundUser = _context.Users.Where(u => u.Id == id).Single();

            if (foundUser.Role.Equals("Administrator"))
            {
                return View();
            }
            else
            {
                return RedirectToAction("Logout", "Users");
            }
        }

        // POST: Providers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(HireProviderViewModel model)
        {
            if (ModelState.IsValid)
            {
                var provider = new Providers
                {
                    Name = model.Name,
                    Service = model.Service,
                    Address = model.Address
                };
                _context.Add(provider);
                await _context.SaveChangesAsync();
                var providerContract = new ProviderContracts
                {
                    ProviderId = provider.Id,
                    Date = DateTime.Now,
                    ExpirationDate = model.ExpirationDate,
                    Price = model.Price
                };

                _context.ProviderContracts.Add(providerContract);
                await _context.SaveChangesAsync();
                return RedirectToAction("SeeIncomeAndCosts", "Bills");
            }
            ViewData["Message"] = "The data is not valid. Try again...";
            return View(model);
        }

        //GET: Providers/Edit/5
        public IActionResult Edit()
        {
            var sessionContent = HttpContext.Session.GetString("loggedInUser");
            // check if a user is logged
            if (sessionContent == null)
            {
                return RedirectToAction("Index", "Home");
            }
            // get the details of the logged in user
            var user = JsonConvert.DeserializeObject<Users>(sessionContent);
            int id = user.Id;
            var foundUser = _context.Users.Where(u => u.Id == id).Single();

            if (foundUser.Role.Equals("Administrator"))
            {
                ViewData["Name"] = new SelectList(_context.Providers, "Name", "Name");
                return View();
            }
            else
            {
                return RedirectToAction("Logout", "Users");
            }
        }

        // POST: Providers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(HireProviderViewModel model)
        {
            Providers searchedProvider = _context.Providers.Where(p => p.Name.Equals(model.Name)).FirstOrDefault();
            if (searchedProvider != null)
            {
                ProviderContracts searchedContract = _context.ProviderContracts.Where(c => c.ProviderId == searchedProvider.Id).FirstOrDefault();
                searchedProvider.Name = (model.Name != null && !model.Name.Equals(searchedProvider.Name)) ? model.Name : searchedProvider.Name;
                searchedProvider.Service = (model.Service != null && !model.Service.Equals(searchedProvider.Service)) ? model.Service : searchedProvider.Service;
                searchedProvider.Address = (model.Address != null && !model.Address.Equals(searchedProvider.Address)) ? model.Address : searchedProvider.Address;
                _context.Update(searchedProvider);
                searchedContract.ExpirationDate = model.ExpirationDate != null ? model.ExpirationDate : searchedContract.ExpirationDate;
                searchedContract.Price = model.Price != 0 ? model.Price : searchedContract.Price;
                _context.Update(searchedContract);
                await _context.SaveChangesAsync();
            }
            else
            {
                ViewData["Message"] = "The data is not valid. Try again...";
            }
            return RedirectToAction("SeeIncomeAndCosts", "Bills");
        }

        public IActionResult GetProviderDetails(string id)
        {
            var provider = _context.Providers.Where(p => p.Name.Equals(id)).FirstOrDefault();
            // search the contracts
            var contract = _context.ProviderContracts.Where(pc => pc.ProviderId == provider.Id).FirstOrDefault();
            var providerViewModel = new HireProviderViewModel
            {
                Name = provider.Name,
                Service = provider.Service,
                Address = provider.Address,
                ExpirationDate = contract.ExpirationDate,
                Price = contract.Price
            };
            return Json(providerViewModel);
        }

        // GET: Providers/Delete/5
        public IActionResult Delete()
        {
            var sessionContent = HttpContext.Session.GetString("loggedInUser");
            // check if a user is logged
            if (sessionContent == null)
            {
                return RedirectToAction("Index", "Home");
            }
            // get the details of the logged in user
            var user = JsonConvert.DeserializeObject<Users>(sessionContent);
            int id = user.Id;
            var foundUser = _context.Users.Where(u => u.Id == id).Single();

            if (foundUser.Role.Equals("Administrator"))
            {
                ViewData["Name"] = new SelectList(_context.Providers, "Name", "Name");
                return View();
            }
            else
            {
                return RedirectToAction("Logout", "Users");
            }
        }

        // POST: Providers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string name)
        {
            var providers = _context.Providers.Where(p => p.Name.Equals(name)).First();
            _context.Providers.Remove(providers);
            await _context.SaveChangesAsync();
            return RedirectToAction("SeeIncomeAndCosts", "Bills");
        }
    }
}
