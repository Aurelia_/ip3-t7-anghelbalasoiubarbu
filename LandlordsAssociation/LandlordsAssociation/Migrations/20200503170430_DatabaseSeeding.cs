﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LandlordsAssociation.Migrations
{
    public partial class DatabaseSeeding : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "BlockOfFlats",
                columns: new[] { "Id", "City", "Street" },
                values: new object[,]
                {
                    { 1, "Craiova", "Petre Ispirescu" },
                    { 2, "Craiova", "Petre Ispirescu" },
                    { 3, "Craiova", "Calea Bucuresti" }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Password", "Role", "Username" },
                values: new object[] { 1, "LandlordsAssociation@la", "Administrator", "la_admin@la.com" });

            migrationBuilder.InsertData(
                table: "Apartments",
                columns: new[] { "Id", "BlockId", "ContractId", "Entrance", "Floor", "Number", "NumberPersons", "NumberRooms" },
                values: new object[,]
                {
                    { 1, 1, null, "A", 0, 1, 0, 3 },
                    { 25, 2, null, "B", 0, 1, 0, 2 },
                    { 26, 2, null, "B", 0, 2, 0, 2 },
                    { 27, 2, null, "B", 0, 3, 0, 2 },
                    { 28, 2, null, "B", 0, 4, 0, 2 },
                    { 29, 2, null, "B", 1, 5, 0, 2 },
                    { 30, 2, null, "B", 1, 6, 0, 2 },
                    { 31, 2, null, "B", 1, 7, 0, 2 },
                    { 32, 2, null, "B", 1, 8, 0, 2 },
                    { 33, 3, null, "A", 0, 1, 0, 4 },
                    { 34, 3, null, "A", 0, 2, 0, 4 },
                    { 35, 3, null, "A", 0, 3, 0, 4 },
                    { 36, 3, null, "A", 1, 4, 0, 4 },
                    { 37, 3, null, "A", 1, 5, 0, 4 },
                    { 38, 3, null, "A", 1, 6, 0, 4 },
                    { 39, 3, null, "B", 0, 1, 0, 4 },
                    { 40, 3, null, "B", 0, 2, 0, 4 },
                    { 41, 3, null, "B", 0, 3, 0, 4 },
                    { 42, 3, null, "B", 1, 4, 0, 4 },
                    { 43, 3, null, "B", 1, 5, 0, 4 },
                    { 24, 2, null, "A", 1, 8, 0, 1 },
                    { 44, 3, null, "B", 1, 6, 0, 4 },
                    { 23, 2, null, "A", 1, 7, 0, 1 },
                    { 21, 2, null, "A", 1, 5, 0, 1 },
                    { 2, 1, null, "A", 0, 2, 0, 3 },
                    { 3, 1, null, "A", 0, 3, 0, 3 },
                    { 4, 1, null, "A", 0, 4, 0, 3 },
                    { 5, 1, null, "A", 1, 5, 0, 3 },
                    { 6, 1, null, "A", 1, 6, 0, 3 },
                    { 7, 1, null, "A", 1, 7, 0, 3 },
                    { 8, 1, null, "A", 1, 8, 0, 3 },
                    { 9, 1, null, "B", 0, 1, 0, 2 },
                    { 10, 1, null, "B", 0, 2, 0, 2 },
                    { 11, 1, null, "B", 0, 3, 0, 2 },
                    { 12, 1, null, "B", 0, 4, 0, 2 },
                    { 13, 1, null, "B", 1, 5, 0, 2 },
                    { 14, 1, null, "B", 1, 6, 0, 2 },
                    { 15, 1, null, "B", 1, 7, 0, 2 },
                    { 16, 1, null, "B", 1, 8, 0, 2 },
                    { 17, 2, null, "A", 0, 1, 0, 1 },
                    { 18, 2, null, "A", 0, 2, 0, 1 },
                    { 19, 2, null, "A", 0, 3, 0, 1 },
                    { 20, 2, null, "A", 0, 4, 0, 1 },
                    { 22, 2, null, "A", 1, 6, 0, 1 }
                });

            migrationBuilder.InsertData(
                table: "Employees",
                columns: new[] { "CNP", "Address", "Email", "FirstName", "Job", "LastName", "PhoneNumber", "Salary", "UserId" },
                values: new object[] { "1212122343445", "-", "la_admin@la.com", "L", "Secretary", "A", "-", 0f, 1 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Apartments",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Apartments",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Apartments",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Apartments",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Apartments",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Apartments",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Apartments",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Apartments",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Apartments",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Apartments",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Apartments",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Apartments",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Apartments",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Apartments",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Apartments",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Apartments",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "Apartments",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "Apartments",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "Apartments",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "Apartments",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "Apartments",
                keyColumn: "Id",
                keyValue: 21);

            migrationBuilder.DeleteData(
                table: "Apartments",
                keyColumn: "Id",
                keyValue: 22);

            migrationBuilder.DeleteData(
                table: "Apartments",
                keyColumn: "Id",
                keyValue: 23);

            migrationBuilder.DeleteData(
                table: "Apartments",
                keyColumn: "Id",
                keyValue: 24);

            migrationBuilder.DeleteData(
                table: "Apartments",
                keyColumn: "Id",
                keyValue: 25);

            migrationBuilder.DeleteData(
                table: "Apartments",
                keyColumn: "Id",
                keyValue: 26);

            migrationBuilder.DeleteData(
                table: "Apartments",
                keyColumn: "Id",
                keyValue: 27);

            migrationBuilder.DeleteData(
                table: "Apartments",
                keyColumn: "Id",
                keyValue: 28);

            migrationBuilder.DeleteData(
                table: "Apartments",
                keyColumn: "Id",
                keyValue: 29);

            migrationBuilder.DeleteData(
                table: "Apartments",
                keyColumn: "Id",
                keyValue: 30);

            migrationBuilder.DeleteData(
                table: "Apartments",
                keyColumn: "Id",
                keyValue: 31);

            migrationBuilder.DeleteData(
                table: "Apartments",
                keyColumn: "Id",
                keyValue: 32);

            migrationBuilder.DeleteData(
                table: "Apartments",
                keyColumn: "Id",
                keyValue: 33);

            migrationBuilder.DeleteData(
                table: "Apartments",
                keyColumn: "Id",
                keyValue: 34);

            migrationBuilder.DeleteData(
                table: "Apartments",
                keyColumn: "Id",
                keyValue: 35);

            migrationBuilder.DeleteData(
                table: "Apartments",
                keyColumn: "Id",
                keyValue: 36);

            migrationBuilder.DeleteData(
                table: "Apartments",
                keyColumn: "Id",
                keyValue: 37);

            migrationBuilder.DeleteData(
                table: "Apartments",
                keyColumn: "Id",
                keyValue: 38);

            migrationBuilder.DeleteData(
                table: "Apartments",
                keyColumn: "Id",
                keyValue: 39);

            migrationBuilder.DeleteData(
                table: "Apartments",
                keyColumn: "Id",
                keyValue: 40);

            migrationBuilder.DeleteData(
                table: "Apartments",
                keyColumn: "Id",
                keyValue: 41);

            migrationBuilder.DeleteData(
                table: "Apartments",
                keyColumn: "Id",
                keyValue: 42);

            migrationBuilder.DeleteData(
                table: "Apartments",
                keyColumn: "Id",
                keyValue: 43);

            migrationBuilder.DeleteData(
                table: "Apartments",
                keyColumn: "Id",
                keyValue: 44);

            migrationBuilder.DeleteData(
                table: "Employees",
                keyColumn: "CNP",
                keyValue: "1212122343445");

            migrationBuilder.DeleteData(
                table: "BlockOfFlats",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "BlockOfFlats",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "BlockOfFlats",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);
        }
    }
}
