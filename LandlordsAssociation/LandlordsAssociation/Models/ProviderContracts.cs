﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LandlordsAssociation.Models
{
    public class ProviderContracts
    {
        [Key]
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public DateTime ExpirationDate { get; set; }
        public float Price { get; set; }

        // Navigation properties
        [ForeignKey("Providers")]
        public int ProviderId { get; set; }
        public Providers Provider { get; set; }
    }
}
