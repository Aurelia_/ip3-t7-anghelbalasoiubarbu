-- Prima data:
--create database LandlordsAssociation;
use LandlordsAssociation;

-- Dupa:
/*create table Users(
	Id nvarchar(6) not null Primary Key,
	Username nvarchar(20) not null,
	Password nvarchar(20) not null,
	Role nvarchar(5) not null
);
create table Employees(
	CNP nvarchar(13) not null Primary Key,
	FirstName nvarchar(20) not null,
	LastName nvarchar(20) not null,
	Salary float not null,
	Address nvarchar(100),
	Email nvarchar(40) not null,
	PhoneNumber nvarchar(10),
	Job nvarchar(50) not null,
	UserId nvarchar(6)
);
create table Providers(
	Id int not null identity(1,1) Primary Key,
	Name nvarchar(40) not null,
	Service nvarchar(40) not null,
	Address nvarchar(100)
);
create table Tenants(
	CNP nvarchar(13) not null primary key,
	FirstName nvarchar(20) not null,
	LastName nvarchar(20) not null,
	PhoneNumber nvarchar(10)
);
create table BlockOfFlats(
	Id int not null primary key identity(1, 1),
	Street nvarchar(15) not null,
	City nvarchar(15) not null
);
create table AdditionalBills(
	Id int not null primary key identity(1, 1),
	Service nvarchar(20) not null,
	Value float not null,
	Date DateTime not null
);
create table TenantContracts(
	Id int not null primary key identity(1, 1),
	TenantCNP nvarchar(13) not null,
	Type nvarchar(6) not null,
	Value float not null
);
create table Apartments(
	Id int not null primary key identity(1, 1),
	NumberRooms smallint not null,
	NumberPeople smallint not null,
	Floor smallint not null,
	BlockId int not null,
	ContractId int not null
);
create table ProviderBills(
	Id int not null primary key identity(1, 1),
	ProviderId int not null,
	ApartmentId int not null,
	Value float not null,
	Paid nvarchar(5) not null,
	Quantity float not null,
	Date DateTime not null
);
create table ProviderContracts(
	Id int not null primary key identity(1, 1),
	Date DateTime not null,
	ExpirationDate DateTime not null,
	Price float not null,
	ProviderId int not null
);

create table WaterConsumption(
	Id int not null primary key identity(1, 1),
	ApartmentId int not null,
	Value float not null,
	LastReadValue float not null,
	Date DateTime not null
);
*/

-- Pentru legaturile dintre tabele:
--alter table Employees add foreign key(UserId) references Users(Id);
--alter table ProviderContracts add foreign key(ProviderId) references Providers(Id);
--alter table ProviderBills add foreign key(ProviderId) references Providers(Id);
--alter table ProviderBills add foreign key(ApartmentId) references Apartments(Id);
--alter table Apartments add TenantCNP nvarchar(13) not null;
--alter table Apartments add Entrance char not null;
--alter table Apartments add foreign key(TenantCNP) references Tenants(CNP);
--alter table Apartments add foreign key(BlockId) references BlockOfFlats(Id);
--alter table Apartments add foreign key(ContractId) references TenantContracts(Id);
--alter table WaterConsumption add foreign key(ApartmentId) references Apartments(Id);
--alter table WaterConsumption add foreign key(ApartmentId) references Apartments(Id);