﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using LandlordsAssociation.Data;
using LandlordsAssociation.Models;
using LandlordsAssociation.ViewModels;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace LandlordsAssociation.Controllers
{
    public class ApartmentsController : Controller
    {
        private readonly LandlordsAssociationDbContext _context;

        public ApartmentsController(LandlordsAssociationDbContext context)
        {
            _context = context;
        }
        
        // Get: Apartments/SeeDebtForApartment
        [HttpGet]
        public IActionResult SeeDebtForApartment()
        {
            var sessionContent = HttpContext.Session.GetString("loggedInUser");
            // check if a user is logged
            if (sessionContent == null)
            {
                return RedirectToAction("Index", "Home");
            }
            // get the details of the logged in user
            var user = JsonConvert.DeserializeObject<Users>(sessionContent);
            int id = user.Id;
            var foundUser = _context.Users.Where(u => u.Id == id).Single();

            if (foundUser.Role.Equals("User"))
            {
                var blocks = _context.BlockOfFlats.ToList().Distinct();
                ViewData["Blocks"] = new SelectList(blocks, "Id", "Id");
                DebtForApartmentViewModel model = new DebtForApartmentViewModel();
                return View(model);
            }
            else
            {
                return RedirectToAction("Logout", "Users");
            }
        }


        // Post: Apartments/SeeDebtForApartment
        [HttpPost, ActionName("SeeDebtForApartment")]
        public IActionResult SeeDebtForApartment(DebtForApartmentViewModel model)
        {
            if (ModelState.IsValid)
            {
                var apartment = (from a in _context.Apartments
                                 join block in _context.BlockOfFlats on a.BlockId equals block.Id
                                 where block.Id == model.BlockNumber && a.Entrance.Equals(model.Entrance)
                                 && a.Number == model.Number
                                 select a).SingleOrDefault();
                if(apartment != null)
                {
                    return RedirectToAction("SeeResultsApartment", "Apartments", new { id = apartment.Id });
                }
                else
                {
                    var blocks = _context.BlockOfFlats.ToList().Distinct();
                    ViewData["Blocks"] = new SelectList(blocks, "Id", "Id");
                    ViewData["Message"] = "The data is not valid. Try again...";
                    return View();
                }
            }
            else
            {
                var blocks = _context.BlockOfFlats.ToList().Distinct();
                ViewData["Blocks"] = new SelectList(blocks, "Id", "Id");
                ViewData["Message"] = "The data is not valid. Try again...";
                return View();
            }
            
        }

        [HttpGet]
        public IActionResult SeeResultsApartment(int? id)
        {
            var sessionContent = HttpContext.Session.GetString("loggedInUser");
            // check if a user is logged
            if (sessionContent == null)
            {
                return RedirectToAction("Index", "Home");
            }
            // get the details of the logged in user
            var user = JsonConvert.DeserializeObject<Users>(sessionContent);
            int userid = user.Id;
            var foundUser = _context.Users.Where(u => u.Id == userid).Single();

            if (foundUser.Role.Equals("User"))
            {
                if (id != null)
                {
                    var apartment = _context.Apartments.Where(a => a.Id == id).SingleOrDefault();
                    var totalNumberOfRooms = (from a in _context.Apartments
                                              select a.NumberRooms).Sum();
                    var salaries = (from employee in _context.Employees
                                    select employee.Salary).Sum();
                    var providerBills = (from b in _context.ProviderBills
                                        join pc in _context.ProviderContracts on b.ProviderId equals pc.ProviderId
                                        where b.ApartmentId == apartment.Id && b.Paid == false
                                        select b.Value).Sum();
                    var numberPersons = apartment.NumberPersons;
                    var numberRooms = apartment.NumberRooms;
                    var debt = (providerBills + salaries) / totalNumberOfRooms * numberRooms * (numberPersons * 0.5);
                   
                    var unpaidRent = (from contract in _context.TenantContracts
                                      join ap in _context.Apartments on contract.Id equals ap.ContractId
                                      join rent in _context.Rent on ap.Id equals rent.ApartmentId
                                      where (contract.Type.Equals("Rent")
                                             && rent.IsPaid == false
                                             && apartment.Id == ap.Id)
                                      select contract.Value).Sum();
                    var unpaidRentCurrentMonth = (from contract in _context.TenantContracts
                                      join ap in _context.Apartments on contract.Id equals ap.ContractId
                                      join rent in _context.Rent on ap.Id equals rent.ApartmentId
                                      where (contract.Type.Equals("Rent")
                                             && rent.IsPaid == false
                                             && apartment.Id == ap.Id
                                             && EF.Functions.DateDiffMonth(DateTime.Now, rent.PayDate) == 0)
                                      select contract.Value).Sum();
                    var waterBill = (from bill in _context.WaterConsumption
                                     join a in _context.Apartments on bill.ApartmentId equals a.Id
                                     where a.Id == apartment.Id
                                     select bill.Value - bill.LastReadValue).Sum();
                    waterBill *= (from contract in _context.ProviderContracts
                                  join provider in _context.Providers on contract.ProviderId equals provider.Id
                                  where provider.Service.Equals("water")
                                  select contract.Price).SingleOrDefault();
                    var payment = (from p in _context.Payments
                                   where p.ApartmentId == apartment.Id && EF.Functions.DateDiffMonth(p.Date, DateTime.Today) == 0
                                   select p).SingleOrDefault();
                    
                    if(payment != null)
                    {
                        debt = 0;
                    }
                    ApartmentDebtViewModel apartmentDebtViewModel = new ApartmentDebtViewModel
                    {
                        Apartment = apartment,
                        Debt = debt,
                        UnpaidRent = unpaidRent,
                        WaterBill = waterBill
                    };

                    var totalAmount = unpaidRent + debt;
                    ViewData["TotalAmount"] = totalAmount;
                    ViewData["CurrentMonth"] = debt + unpaidRent;

                    return View(apartmentDebtViewModel);

                }
                else
                {
                    return RedirectToAction("SeeDebtList", "Bills");
                }
            }
            else
            {
                return RedirectToAction("Logout", "Users");
            }
        }

        // GET: Apartments/UpdateNumberOfPersons
        public IActionResult UpdateNumberOfPersons()
        {
            var sessionContent = HttpContext.Session.GetString("loggedInUser");
            // check if a user is logged
            if (sessionContent == null)
            {
                return RedirectToAction("Index", "Home");
            }
            // get the details of the logged in user
            var user = JsonConvert.DeserializeObject<Users>(sessionContent);
            int id = user.Id;
            var foundUser = _context.Users.Where(u => u.Id == id).Single();

            if (foundUser.Role.Equals("User"))
            {
                ChangeNumberOfPersonsViewModel model = new ChangeNumberOfPersonsViewModel();
                var blocks = _context.BlockOfFlats.ToList();
                ViewData["Blocks"] = new SelectList(blocks, "Id", "Id");
                return View(model);
            }
            else
            {
                return RedirectToAction("Logout", "Users");
            }

        }

        // POST: Apartments/UpdateNumberOfPersons
        [HttpPost, ActionName("UpdateNumberOfPersons")]
        public IActionResult UpdateNumberOfPersons(ChangeNumberOfPersonsViewModel model)
        {
            var blocks = _context.BlockOfFlats.ToList();
            if (ModelState.IsValid)
            {
                var foundApartment = (from a in _context.Apartments
                                      join b in _context.BlockOfFlats on a.BlockId equals b.Id
                                      where a.Entrance.Equals(model.Entrance) 
                                            && model.ApartmentNumber == a.Number
                                            && model.BlockId == b.Id
                                      select a
                                      ).SingleOrDefault();
                if (foundApartment != null)
                {
                    foundApartment.NumberPersons = model.NumberOfPersons;
                    _context.Apartments.Update(foundApartment);
                    _context.SaveChanges();
                    return RedirectToAction("ShowDebtList", "Bills");
                }
                else
                {
                    ViewData["Message"] = "The data is not valid. Try again...";
                    ViewData["Blocks"] = new SelectList(blocks, "Id", "Id");
                    return View();
                }

            }
            ViewData["Message"] = "The data is not valid. Try again...";
            ViewData["Blocks"] = new SelectList(blocks, "Id", "Id");
            return View();
        }

        [HttpGet]
        public IActionResult AssignApartmentToTenant()
        {
            var sessionContent = HttpContext.Session.GetString("loggedInUser");
            if (sessionContent == null)
            {
                return RedirectToAction("Index", "Home");
            }
            var user = JsonConvert.DeserializeObject<Users>(sessionContent);
            int id = user.Id;
            var foundUser = _context.Users.Where(u => u.Id == id).Single();

            if (foundUser.Role.Equals("Administrator"))
            {
                ViewData["BlockId"] = new SelectList(_context.BlockOfFlats, "Id", "Id");
                ViewData["TenantCNP"] = new SelectList(_context.Tenants, "CNP", "CNP");
                return View();
            }
            else
            {
                return RedirectToAction("Logout", "Users");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AssignApartmentToTenant(TenantContractViewModel model)
        {
            if (ModelState.IsValid)
            {
                Apartments apartment = _context.Apartments.Where(a => (a.Number == model.ApartmentNumber) && (a.BlockId == model.BlockNumber)
                && (a.Entrance == model.Entrance) && (a.ContractId == null)).FirstOrDefault();
                if (apartment != null)
                {
                    TenantContracts contract = new TenantContracts
                    {
                        Type = model.ContractType,
                        Value = model.Value,
                        Date = DateTime.Today,
                        TenantCNP = model.TenantCNP
                    };
                    _context.TenantContracts.Add(contract);
                    await _context.SaveChangesAsync();
                    apartment.ContractId = contract.Id;
                    apartment.NumberPersons = model.NoPersons;
                    _context.Apartments.Update(apartment);
                    await _context.SaveChangesAsync();
                    return RedirectToAction("SeeIncomeAndCosts", "Bills");
                }
            }
            ViewData["BlockId"] = new SelectList(_context.BlockOfFlats, "Id", "Id");
            ViewData["TenantCNP"] = new SelectList(_context.Tenants, "CNP", "CNP");
            ViewData["Message"] = "The data is not valid. Try again...";
            return View();
        }
        public IActionResult GetApartmentRooms(int apartmentNumber, int blockNumber, string entranceName)
        {
            var apartment = _context.Apartments.Where(a => (a.Number == apartmentNumber) && (a.BlockId == blockNumber) && (String.Equals(a.Entrance, entranceName))).FirstOrDefault();
            return Json(apartment.NumberRooms);
        }
    }
}
