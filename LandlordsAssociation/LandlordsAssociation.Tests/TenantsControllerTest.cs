﻿using LandlordsAssociation.Data;
using LandlordsAssociation.Models;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace LandlordsAssociation.Tests
{
    public class TenantsControllerTest
    {
        private LandlordsAssociationDbContext context;

        public TenantsControllerTest()
        {
            var options = new DbContextOptionsBuilder<LandlordsAssociationDbContext>()
                .UseInMemoryDatabase(databaseName: "LandlordsAssociationTestDatabase")
                .Options;
            context = new LandlordsAssociationDbContext(options);
        }

        [Fact]
        public void AddTenant_TenantData_Succeeds()
        {
            var tenant = new Tenants
            {
                CNP = "2710107876543",
                FirstName = "Marius",
                LastName = "Popescu",
                PhoneNumber = "0765445451"
            };
            context.Tenants.Add(tenant);
            context.SaveChanges();
            Assert.NotNull(context.Tenants.Find("2710107876543"));
        }
    }
}
