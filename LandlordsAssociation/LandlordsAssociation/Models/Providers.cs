﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LandlordsAssociation.Models
{
    public class Providers
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Service { get; set; }
        public string Address { get; set; }

        // Navigation properties
        public ICollection<ProviderContracts> ProviderContracts { get; set; }
        public ICollection<ProviderBills> ProviderBills { get; set; }
    }
}
