﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LandlordsAssociation.Models
{
    public class TenantContracts
    {
        [Key]
        public int Id { get; set; }
        public string Type { get; set; }
        public float Value { get; set; }
        public  DateTime Date { get; set; }

        [ForeignKey("Tenants")]
        public string TenantCNP { get; set; }

        // Navigation properties
        public Apartments Apartment { get; set; }
        public Tenants Tenant { get; set; }
    }
}
