﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LandlordsAssociation.ViewModels
{
    public class TenantContractViewModel
    {
        [RegularExpression(@"(1|2)[0-9]{12}")]
        [StringLength(13, MinimumLength = 13, ErrorMessage = "The CNP must have 13 digits and it must start with 1 or 2")]
        public string TenantCNP { get; set; }
        [Range(0, 10, ErrorMessage = "The number of persons must be in the interval [0, 10].")]
        public int NoPersons { get; set; }
        [Range(1, 4, ErrorMessage = "The number of rooms must be in the interval [1, 4].")]
        public int NoRooms { get; set; }
        public string ContractType { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "The value must be positive.")]
        public float Value { get; set; }
        [Range(1, 30, ErrorMessage = "The apartment number must be positive.")]
        public int ApartmentNumber { get; set; }
        public int BlockNumber { get; set; }
        public string Entrance { get; set; }
    }
}
