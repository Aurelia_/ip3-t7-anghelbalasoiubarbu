﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LandlordsAssociation.Migrations
{
    public partial class UpdateApartment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Apartments_Tenants_TenantCNP",
                table: "Apartments");

            migrationBuilder.DropIndex(
                name: "IX_Apartments_TenantCNP",
                table: "Apartments");

            migrationBuilder.DropColumn(
                name: "TenantCNP",
                table: "Apartments");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Apartments_Tenants_TenantsCNP",
                table: "Apartments");

            migrationBuilder.DropIndex(
                name: "IX_Apartments_TenantsCNP",
                table: "Apartments");

            migrationBuilder.DropColumn(
                name: "TenantsCNP",
                table: "Apartments");

            migrationBuilder.AddColumn<string>(
                name: "TenantCNP",
                table: "Apartments",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Apartments_TenantCNP",
                table: "Apartments",
                column: "TenantCNP");

            migrationBuilder.AddForeignKey(
                name: "FK_Apartments_Tenants_TenantCNP",
                table: "Apartments",
                column: "TenantCNP",
                principalTable: "Tenants",
                principalColumn: "CNP",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
