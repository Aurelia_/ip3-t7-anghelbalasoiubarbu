﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using LandlordsAssociation.Data;
using LandlordsAssociation.Models;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using LandlordsAssociation.ViewModels;

namespace LandlordsAssociation.Controllers
{
    public class BillsController : Controller
    {
        private readonly LandlordsAssociationDbContext _context;

        public BillsController(LandlordsAssociationDbContext context)
        {
            _context = context;
        }

        //GET/Bills/SeeIncomeAndCosts
        public IActionResult SeeIncomeAndCosts()
        {
            var sessionContent = HttpContext.Session.GetString("loggedInUser");
            // check if a user is logged
            if (sessionContent == null)
            {
                return RedirectToAction("Index", "Home");
            }
            //get the details of the logged in user
            var user = JsonConvert.DeserializeObject<Users>(sessionContent);
            int id = user.Id;
            var foundUser = _context.Users.Where(u => u.Id == id).Single();

            if (foundUser.Role.Equals("Administrator"))
            {
                var apartments = (from apartment in _context.Apartments
                                  join contract in _context.TenantContracts on apartment.ContractId equals contract.Id
                                  where contract.TenantCNP != null
                                  select apartment).ToList();
                List<double> incomes = new List<double>();
                var totalNumberOfRooms = (from a in _context.Apartments
                                          select a.NumberRooms).Sum();
                var salaries = (from employee in _context.Employees
                                select employee.Salary).Sum();
                foreach (var apartment in apartments)
                {
                    var providersIncome = (from b in _context.ProviderBills
                                           join pc in _context.ProviderContracts on b.ProviderId equals pc.ProviderId
                                           where b.ApartmentId == apartment.Id && b.Paid == true
                                           select b.Value).Sum();
                    var numberPersons = apartment.NumberPersons;
                    var numberRooms = apartment.NumberRooms;
                    double income = 0;
                    if(providersIncome > 0)
                    {
                        income = (providersIncome + salaries) / totalNumberOfRooms * numberRooms * (numberPersons * 0.5);
                    }
                    
                    incomes.Add(income);
                }
                var incomeFromContracts = (from contract in _context.TenantContracts
                                           join apartment in _context.Apartments on contract.Id equals apartment.ContractId
                                           join rent in _context.Rent on apartment.Id equals rent.ApartmentId
                                           where (contract.Type.Equals("Rent")
                                                 && rent.IsPaid == true
                                                 && EF.Functions.DateDiffMonth(rent.PayDate, DateTime.Today) == 0)     
                                           select contract.Value).Sum();
                var incomeSales = (from contract in _context.TenantContracts
                                   where contract.Type.Equals("Buy") && EF.Functions.DateDiffMonth(contract.Date, DateTime.Today) == 0
                                   select contract.Value).Sum();
                double totalIncomeFromApartments = 0;
                foreach (var income in incomes)
                {
                    totalIncomeFromApartments += income;
                }
                var totalIncome = totalIncomeFromApartments + incomeFromContracts;
               
                ViewData["IncomeContracts"] = String.Format("{0:00.00 }", incomeFromContracts + incomeSales);// sales/rentals
                ViewData["IncomeApartments"] = String.Format("{0:00.00 }", totalIncomeFromApartments); // bills for tenants
                ViewData["TotalIncome"] = String.Format("{0:00.00 }", totalIncome + incomeSales); // total income

                //Compute the expenses:
                var expensesBills = (from bill in _context.ProviderBills
                                     join contract in _context.ProviderContracts on bill.ProviderId equals contract.ProviderId
                                     where bill.Paid == true && EF.Functions.DateDiffMonth(bill.Date, DateTime.Today) == 0
                                     select bill.Value)
                                     .Sum();
                var additionalBills = (from bill in _context.AdditionalBills
                                       where EF.Functions.DateDiffMonth(bill.Date, DateTime.Now) == 0
                                       select bill.Value).Sum();
                var totalExpenses = salaries + expensesBills + additionalBills;

                ViewData["ExpensesBills"] = String.Format("{0:00.00 }", expensesBills); // bills from providers
                ViewData["AdditionalBills"] = String.Format("{0:00.00 }", additionalBills); // unexpected payments
                ViewData["Salaries"] = String.Format("{0:00.00 }", salaries); // salaries
                ViewData["TotalExpenses"] = String.Format("{0:00.00 }", totalExpenses); // total

                //Compute the profit:
                var profit = totalIncome + incomeSales - totalExpenses;
                ViewData["Profit"] =  profit; // profit

                var noEmployees = _context.Employees.Count();
                var noProviders = _context.Providers.Count();
                var noFreeFlats = _context.Apartments.Where(a => a.ContractId == null).Count();
                var noTenants = _context.Tenants.Count();
                ViewData["NoEmployees"] = noEmployees;
                ViewData["NoProviders"] = noProviders;
                ViewData["NoFlats"] = noFreeFlats;
                ViewData["NoTenants"] = noTenants;
                return View();
            }
            else
            {
                return RedirectToAction("Logout", "Users");
            }
        }

        public IActionResult ShowDebtList()
        {
            var sessionContent = HttpContext.Session.GetString("loggedInUser");
            // check if a user is logged
            if (sessionContent == null)
            {
                return RedirectToAction("Index", "Home");
            }
            // get the details of the logged in user
            var user = JsonConvert.DeserializeObject<Users>(sessionContent);
            int id = user.Id;
            var foundUser = _context.Users.Where(u => u.Id == id).Single();

            if (foundUser.Role.Equals("User"))
            {
                var apartments = _context.Apartments.Where(a => a.ContractId != null).Include(a => a.Block).ToList();
                List<ApartmentDebtViewModel> debts = new List<ApartmentDebtViewModel>();
                var totalNumberOfRooms = (from a in _context.Apartments
                                          select a.NumberRooms).Sum();
                var salaries = (from employee in _context.Employees
                                select employee.Salary).Sum();
                foreach (var apartment in apartments)
                {
                    var providerBills = (from a in _context.Apartments
                                         join bill in _context.ProviderBills on a.Id equals bill.ApartmentId
                                         join provider in _context.Providers on bill.ProviderId equals provider.Id
                                         join contract in _context.ProviderContracts on provider.Id equals contract.ProviderId
                                         where bill.Paid == false && a.Id == apartment.Id
                                         select bill.Value).Sum();
                    var numberPersons = (from a in _context.Apartments
                                         where a.Id == apartment.Id
                                         select a.NumberPersons).Single();
                    var numberRooms = (from a in _context.Apartments
                                       where a.Id == apartment.Id
                                       select a.NumberRooms).Single();
                    var debt = (providerBills + salaries) / totalNumberOfRooms * numberRooms * (numberPersons * 0.5);
                    var unpaidRent = (from contract in _context.TenantContracts
                                      join ap in _context.Apartments on contract.Id equals ap.ContractId
                                      join rent in _context.Rent on ap.Id equals rent.ApartmentId
                                      where (contract.Type.Equals("Rent")
                                             && rent.IsPaid == false
                                             && apartment.Id == ap.Id)
                                      select contract.Value).Sum();
                    var waterBill = (from bill in _context.WaterConsumption
                                     join pbill in _context.ProviderBills on bill.ApartmentId equals pbill.ApartmentId
                                     join provider in _context.Providers on pbill.ProviderId equals provider.Id
                                     join contract in _context.ProviderContracts on provider.Id equals contract.ProviderId
                                     join a in _context.Apartments on bill.ApartmentId equals a.Id
                                     where a.Id == apartment.Id && bill.Date == pbill.Date && pbill.Paid == false 
                                        && (provider.Service.Equals("Water") || provider.Service.Equals("water"))
                                     select bill.Value - bill.LastReadValue).Sum();
                    waterBill *= (from contract in _context.ProviderContracts
                                  join provider in _context.Providers on contract.ProviderId equals provider.Id
                                  where provider.Service.Equals("Water")
                                  select contract.Price).SingleOrDefault();
                    var payment = (from p in _context.Payments
                                   where p.ApartmentId == apartment.Id && EF.Functions.DateDiffMonth(p.Date, DateTime.Today) == 0
                                   select p).SingleOrDefault();

                    if (payment != null)
                    {
                        debt = 0;
                    }
                    
                    ApartmentDebtViewModel apartmentDebtViewModel = new ApartmentDebtViewModel
                    {
                        Apartment = apartment,
                        Debt = debt,
                        UnpaidRent = unpaidRent,
                        WaterBill = waterBill
                    };
                    debts.Add(apartmentDebtViewModel);
                }
                
                return View(debts);
            }
            else
            {
                return RedirectToAction("Logout", "Users");
            }
        }
        
        public IActionResult GetId()
        {
            int id;
            try
            {
                id = (from bill in _context.AdditionalBills
                          select bill.Id).Max() + 1;
            }catch(Exception)
            {
                id = 1;
            }
            
            return Json(id);
        }

        [HttpGet]
        public IActionResult AddAdditionalBill()
        {
            var sessionContent = HttpContext.Session.GetString("loggedInUser");
            // check if a user is logged
            if (sessionContent == null)
            {
                return RedirectToAction("Index", "Home");
            }
            // get the details of the logged in user
            var user = JsonConvert.DeserializeObject<Users>(sessionContent);
            int id = user.Id;
            var foundUser = _context.Users.Where(u => u.Id == id).Single();

            if (foundUser.Role.Equals("User"))
            {
                return View();
            }
            else
            {
                return RedirectToAction("Logout", "Users");
            }

        }

        [HttpPost, ActionName("AddAdditionalBill")]
        public IActionResult AddAdditionalBill([Bind("Id,Value,Service")]AdditionalBills bill)
        {
            if (ModelState.IsValid)
            {
                bill.Date = DateTime.Now;
                _context.AdditionalBills.Add(bill);
                _context.SaveChanges();
                return RedirectToAction("ShowDebtList", "Bills");
            }
            else
            {
                ViewData["Message"] = "The data is not valid. Try again...";
                return View();
            }
        }


        public IActionResult AddWaterBill()
        {
            var sessionContent = HttpContext.Session.GetString("loggedInUser");
            if (sessionContent == null)
            {
                return RedirectToAction("Index", "Home");
            }
            var user = JsonConvert.DeserializeObject<Users>(sessionContent);
            int id = user.Id;
            var foundUser = _context.Users.Where(u => u.Id == id).Single();

            if (foundUser.Role.Equals("User"))
            {
                ViewData["ApartmentId"] = new SelectList(_context.Apartments, "Id", "Id");
                return View();
            }
            else
            {
                return RedirectToAction("Logout", "Users");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddWaterBill([Bind("Id,ApartmentId,Value,LastReadValue")] WaterConsumption waterBill)
        {
            if (ModelState.IsValid)
            {
                if (waterBill.Value >= waterBill.LastReadValue)
                {
                    waterBill.Date = DateTime.Today;
                    _context.WaterConsumption.Add(waterBill);
                    var foundProvider = (from provider in _context.Providers
                                 where provider.Service.Equals("Water")
                                 select provider).SingleOrDefault();
                    var contract = (from c in _context.ProviderContracts
                                    where c.ProviderId == foundProvider.Id
                                    select c).SingleOrDefault();
                    ProviderBills bill = new ProviderBills
                    {
                        ApartmentId = waterBill.ApartmentId,
                        Paid = false,
                        Quantity = waterBill.Value - waterBill.LastReadValue,
                        Date = waterBill.Date,
                        ProviderId = foundProvider.Id,
                        Value = (waterBill.Value - waterBill.LastReadValue) * contract.Price
                    };
                    _context.ProviderBills.Add(bill);

                    await _context.SaveChangesAsync();
                    return RedirectToAction("ShowDebtList", "Bills");
                }
                else
                {
                    ViewData["ApartmentId"] = new SelectList(_context.Apartments, "Id", "Id");
                    ViewData["Message"] = "The data is not valid. Try again...";
                    return View() ;
                }

            }
            ViewData["ApartmentId"] = new SelectList(_context.Apartments, "Id", "Id");

            return View(waterBill);
        }

        [HttpGet]
        public IActionResult AddProviderBill()
        {
            var sessionContent = HttpContext.Session.GetString("loggedInUser");
            if (sessionContent == null)
            {
                return RedirectToAction("Index", "Home");
            }
            var user = JsonConvert.DeserializeObject<Users>(sessionContent);
            int id = user.Id;
            var foundUser = _context.Users.Where(u => u.Id == id).Single();

            if (foundUser.Role.Equals("User"))
            {
                var providers = _context.Providers.ToList().Distinct();
                ViewData["Providers"] = new SelectList(providers, "Name", "Name");
                return View();
            }
            else
            {
                return RedirectToAction("Logout", "Users");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddProviderBill(AddProviderBillViewModel model)
        {
            if (ModelState.IsValid)
            {
                // search the apartment by its number, number of block, entrance and tenant's cnp and if it exists, then add the provider bill
                var block = _context.BlockOfFlats.Where(b => b.Id == model.BlockNumber).FirstOrDefault();
                var contract = _context.TenantContracts.Where(c => c.TenantCNP == model.TenantCNP).FirstOrDefault();
                var service = (from c in _context.ProviderContracts
                                        join provider in _context.Providers on c.ProviderId equals provider.Id
                                        where provider.Name.Equals(model.ProviderName)
                                        select provider.Service).SingleOrDefault();
                if (block != null && contract != null && service!="Water")
                {
                    var apartment = _context.Apartments.Where(a => a.BlockId == block.Id && a.Number == model.ApartmentNumber && a.ContractId == contract.Id).FirstOrDefault();
                    if (apartment != null)
                    {
                        // the value of the bill will be computed by multiplication of the provided service and its quantity
                        var provider = _context.Providers.Where(p => p.Name == model.ProviderName).FirstOrDefault();
                        if (provider != null)
                        {
                            var providerContract = _context.ProviderContracts.Where(p => p.Id == provider.Id).FirstOrDefault();
                            if (providerContract != null)
                            {
                                var quantity = model.Value / providerContract.Price;
                                ProviderBills bill = new ProviderBills
                                {
                                    Id = model.Id,
                                    Value = model.Value,
                                    Paid = false,
                                    Quantity = quantity,
                                    Date = DateTime.Today,
                                    ProviderId = provider.Id,
                                    ApartmentId = apartment.Id,
                                };
                                _context.ProviderBills.Add(bill);
                                await _context.SaveChangesAsync();
                                return RedirectToAction("ShowDebtList", "Bills");
                            }
                        }
                    }
                }else if (service.Equals("Water"))
                {
                    return RedirectToAction("AddWaterBill", "Bills");
                }
            }
            ViewData["Message"] = "The data is not valid. Try again...";
            var providers = _context.Providers.ToList().Distinct();
            ViewData["Providers"] = new SelectList(providers, "Name", "Name");
            return View(model);
        }

        public IActionResult GetProviderBillId()
        {
            if (_context.ProviderBills.Count() == 0)
            {
                return Json(1);
            }
            return Json((from providerBill in _context.ProviderBills
                         select providerBill.Id).Max() + 1);
        }

        [HttpGet]
        public IActionResult AddPayment()
        {
            var sessionContent = HttpContext.Session.GetString("loggedInUser");
            if (sessionContent == null)
            {
                return RedirectToAction("Index", "Home");
            }
            var user = JsonConvert.DeserializeObject<Users>(sessionContent);
            int id = user.Id;
            var foundUser = _context.Users.Where(u => u.Id == id).Single();

            if (foundUser.Role.Equals("User"))
            {
                return View();
            }
            else
            {
                return RedirectToAction("Logout", "Users");
            }
        }

        [HttpPost]
        public async Task<IActionResult> AddPayment(PaymentViewModel model)
        {
            if(ModelState.IsValid)
            {
                var apartment = _context.Apartments.Where(a => (a.Number == model.ApartmentNumber) && 
                (a.BlockId == model.BlockNumber) && (String.Equals(a.Entrance, model.EntranceName))).FirstOrDefault();

                var existingPayment = (from p in _context.Payments
                               where p.ApartmentId == apartment.Id && EF.Functions.DateDiffMonth(p.Date, DateTime.Today) == 0
                               select p).SingleOrDefault();

                if (apartment != null && existingPayment == null)
                {
                    var providerBillsValue = _context.ProviderBills.Where(b => (b.ApartmentId == apartment.Id) && (b.Paid == false)).Sum(b => b.Value);
                    var rents = _context.Rent.Where(r => (r.ApartmentId == apartment.Id) && (r.IsPaid == false)).ToList();
                    var tenantContract = _context.TenantContracts.Where(c => c.Id == apartment.ContractId).FirstOrDefault();

                    var salaries = (from employee in _context.Employees
                                    select employee.Salary).Sum();

                    var rentValue = 0.0;
                    if (rents != null)
                    {
                        foreach (var rent in rents)
                        {
                            rentValue += tenantContract.Value;
                        }
                    }
                    var totalNumberOfRooms = (from a in _context.Apartments
                                              select a.NumberRooms).Sum();
                    var numberRooms = apartment.NumberRooms;
                    var numberPersons = apartment.NumberPersons;
                    var debt = (providerBillsValue + salaries) / totalNumberOfRooms * numberRooms * (numberPersons * 0.5);
                    var toBePaid = debt + rentValue;
                    
                    if (model.Value >= toBePaid - 0.5 || model.Value <= toBePaid + 0.5)
                    {
                        Payment payment = new Payment
                        {
                            ApartmentId = apartment.Id,
                            Value = model.Value,
                            Date = DateTime.Today
                        };
                        _context.Payments.Add(payment);
                        await _context.SaveChangesAsync();
                        // mark all the bills as paid
                        var providerBills = _context.ProviderBills.Where(b => (b.ApartmentId == apartment.Id) && (b.Paid == false));
                        foreach(var bill in providerBills)
                        {
                            bill.Paid = true;
                            _context.ProviderBills.Update(bill);
                        }
                        await _context.SaveChangesAsync();
                        var rentBills = _context.Rent.Where(r => (r.ApartmentId == apartment.Id) && (r.IsPaid == false));
                        foreach(var bill in rentBills)
                        {
                            bill.IsPaid = true;
                            _context.Rent.Update(bill);
                        }
                        await _context.SaveChangesAsync();
                        return RedirectToAction("ShowDebtList", "Bills"); 
                    }
                }
            }
            ViewData["Message"] = "The data is not valid. Try again...";
            return View(model);
        }

        public IActionResult GetPaymentId()
        {
            if(_context.Payments.Count() == 0)
            {
                return Json(1);
            }
            return Json((from payment in _context.Payments select payment.Id).Max() + 1);
        }

        public IActionResult GetLastReadValue(int id)
        {
            var value = (from w in _context.WaterConsumption
                         join a in _context.Apartments on w.ApartmentId equals a.Id
                         where a.Id == id && EF.Functions.DateDiffMonth(w.Date, DateTime.Today) == 1
                         select w.LastReadValue).SingleOrDefault();
            return Json(value);
        }
    }
}
