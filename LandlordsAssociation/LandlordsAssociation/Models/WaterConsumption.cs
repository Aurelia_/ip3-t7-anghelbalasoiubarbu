﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LandlordsAssociation.Models
{
    public class WaterConsumption
    {
        [Key]
        public int Id { get; set; }
        [Range(0, double.MaxValue)]
        public float Value { get; set; }
        [Range(0, double.MaxValue)]
        public float LastReadValue { get; set; }
        public DateTime Date { get; set; }
        [ForeignKey("Apartments")]
        public int ApartmentId { get; set; }

        // Navigation properties
        public Apartments Apartment{ get; set; }
    }
}
