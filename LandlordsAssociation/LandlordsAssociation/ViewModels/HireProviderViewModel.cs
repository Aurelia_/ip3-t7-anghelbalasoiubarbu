﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LandlordsAssociation.ViewModels
{
    public class HireProviderViewModel
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Service { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public DateTime ExpirationDate { get; set; }
        [Required]
        [Range(1, float.MaxValue)]
        public float Price { get; set; }
    }
}
