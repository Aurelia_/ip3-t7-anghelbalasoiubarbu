﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LandlordsAssociation.ViewModels
{
    public class DeleteEmployeeViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        [RegularExpression(@"(1|2)[0-9]{12}")]
        [StringLength(13, MinimumLength = 13, ErrorMessage = "The CNP must have 13 digits and it must start with 1 or 2")]
        public string CNP { get; set; }
    }
}
