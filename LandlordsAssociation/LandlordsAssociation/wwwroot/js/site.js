﻿function get_last_name() {
    var first_name = { id: $("#first_name_field").val() };
    console.log(first_name);
    // make the AJAX request
    $.ajax({
        url: "../Employees/GetLastName",
        type: "POST",
        data: first_name,
        success: function (data) {
            $("#last_name_field").empty();
            $("#last_name_field").append("<option selected=\"selected\">Choose a last name</option>");

            data.forEach(function (element) {
                $("#last_name_field").append("<option>" + element.lastName + "</option>");
            });

            console.log(data);
        },
        error: function () {
            $("#last_name_field").empty();
            console.log("Something went wrong");
        }
    });
}

function get_cnp() {
    var first_name = { id: $("#first_name_field").val() };
    var last_name = { id: $("#last_name_field").val() };
    var name = {
        firstName: $("#first_name_field").val(),
        lastName: $("#last_name_field").val()
    };
    console.log(name);
    // make the AJAX request
    $.ajax({
        url: "../Employees/GetCNP",
        type: "GET",
        data: name,
        success: function (data) {
            $("#cnp_field").empty();
            data.forEach(function (element) {
                $("#cnp_field").append("<option>" + element.cnp + "</option>");
            });
            console.log(data);
        },
        error: function () {
            $("#cnp_field").empty();
            console.log("Something went wrong");
        }
    });
}
function get_employee() {
    var cnp = { cnp: $("#cnp_field").val() };
    console.log(cnp);
    // make the AJAX request
    $.ajax({
        url: "../Employees/GetEmployeeByCNP",
        type: "GET",
        data: cnp,
        success: function (data) {
            $('#first_name_field').val(data.firstName);
            $('#last_name_field').val(data.lastName);
            $('#salary_field').val(data.salary);
            $('#address_field').val(data.address);
            $('#email_field').val(data.email);
            $('#username_field').val(data.email);
            $('#phone_number_field').val(data.phoneNumber);
            $('#job_field').val(data.job);
            console.log(data);
        },
        error: function () {
            console.log("Something went wrong");
        }
    });
}

function get_next_id() {
    // make the AJAX request
    $.ajax({
        url: "../Bills/GetId",
        type: "GET",
        success: function (data) {
            $('#id_field').val(data);
            console.log(data);
        },
        error: function () {
            console.log("Something went wrong");
        }
    });
}

function set_username() {
    if (document.getElementById("job_field").value == "Accountant"
        || document.getElementById("job_field").value == "Block administrator"
        || document.getElementById("job_field").value == "Secretary") {
        console.log(document.getElementById("email_field").value);
        document.getElementById("username_field").value = document.getElementById("email_field").value;
    }
 
}
function change_username() {
    if (document.getElementById("job_field").value != "Accountant"
        && document.getElementById("job_field").value != "Block administrator"
        && document.getElementById("job_field").value != "Secretary") {
        console.log(document.getElementById("job_field").value);
        document.getElementById("username_field").value = " ";
    } else {
        set_username();
    }

}

function get_next_id_provider_bill() {
    $.ajax({
        url: "../Bills/GetProviderBillId",
        type: "GET",
        success: function (data) {
            $('#id_field').val(data);
            console.log(data);
        },
        error: function () {
            console.log("Something went wrong");
        }
    });
}

function get_rooms_number() {
    var apartment_number = { id: $("#selected_apartment").val() };
    var block_number = { id: $("#selected_block").val() };
    var entrance = { id: $("#selected_entrance").val() };
    var apartment_details = {
        apartmentNumber: $("#selected_apartment").val(),
        blockNumber: $("#selected_block").val(),
        entranceName: $("#selected_entrance").val(),
    };
    console.log(apartment_details);
    $.ajax({
        url: "../Apartments/GetApartmentRooms",
        type: "GET",
        data: apartment_details,
        success: function (data) {
            $("#no_rooms").empty();
            $("#no_rooms").val(data);
            console.log(data);
        },
        error: function () {
            console.log("Something went wrong");
        }
    });
}

function get_next_id_payment() {
    $.ajax({
        url: "../Bills/GetPaymentId",
        type: "GET",
        success: function (data) {
            $('#id_field').val(data);
            console.log(data);
        },
        error: function () {
            console.log("Something went wrong");
        }
    });
}


function get_last_read_value() {
    var apartment_id = { id: $("#apartment_field").val() };
    console.log(apartment_id);
    // make the AJAX request
    $.ajax({
        url: "../Bills/GetLastReadValue",
        type: "POST",
        data: apartment_id,
        success: function (data) {
            $("#last_read_value_field").val(data);
            console.log(data);
        },
        error: function () {
            $("#last_read_value_field").empty();
            console.log("Something went wrong");
        }
    });
}