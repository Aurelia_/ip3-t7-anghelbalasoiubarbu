﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using LandlordsAssociation.Data;
using LandlordsAssociation.Models;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;

namespace LandlordsAssociation.Controllers
{
    public class RentsController : Controller
    {
        private readonly LandlordsAssociationDbContext _context;

        public RentsController(LandlordsAssociationDbContext context)
        {
            _context = context;
        }

        //// GET: Rents
        //public async Task<IActionResult> Index()
        //{
        //    var landlordsAssociationDbContext = _context.Rent.Include(r => r.Apartment);
        //    return View(await landlordsAssociationDbContext.ToListAsync());
        //}

        //// GET: Rents/Details/5
        //public async Task<IActionResult> Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var rent = await _context.Rent
        //        .Include(r => r.Apartment)
        //        .FirstOrDefaultAsync(m => m.Id == id);
        //    if (rent == null)
        //    {
        //        return NotFound();
        //    }

        //    return View(rent);
        //}

        // GET: Rents/Create
        public IActionResult Create()
        {
            var sessionContent = HttpContext.Session.GetString("loggedInUser");
            // check if a user is logged
            if (sessionContent == null)
            {
                return RedirectToAction("Index", "Home");
            }
            // get the details of the logged in user
            var user = JsonConvert.DeserializeObject<Users>(sessionContent);
            int id = user.Id;
            var foundUser = _context.Users.Where(u => u.Id == id).Single();

            if (foundUser.Role.Equals("User"))
            {
                ViewData["ApartmentId"] = new SelectList(_context.Apartments, "Id", "Id");
                return View();
            }
            else
            {
                return RedirectToAction("Logout", "Users");
            }

        }

        // POST: Rents/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,PayDate,ApartmentId")] Rent rent)
        {
            var apartment = (from ap in _context.Apartments
                             where ap.ContractId != null && ap.Id == rent.ApartmentId && ap.Contract.Type.Equals("Rent")
                             select ap).SingleOrDefault();
            if (ModelState.IsValid && apartment != null)
            {
                rent.Date = DateTime.Now;
                rent.IsPaid = false;
                _context.Add(rent);
                await _context.SaveChangesAsync();
                return RedirectToAction("ShowDebtList", "Bills");
            }
            else
            {
                ViewData["Message"] = "The data is not valid. Try again...";
            }
            ViewData["ApartmentId"] = new SelectList(_context.Apartments, "Id", "Id", rent.ApartmentId);
            return View(rent);
        }

    }
}
