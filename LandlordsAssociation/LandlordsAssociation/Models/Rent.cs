﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LandlordsAssociation.Models
{
    public class Rent
    {
        [Key]
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public DateTime PayDate { get; set; }
        public bool IsPaid { get; set; }
        [ForeignKey("Apartments")]
        public int? ApartmentId { get; set; }
        public Apartments Apartment { get; set; }
    }
}
