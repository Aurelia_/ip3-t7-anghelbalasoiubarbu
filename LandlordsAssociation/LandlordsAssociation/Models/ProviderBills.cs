﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LandlordsAssociation.Models
{
    public class ProviderBills
    {
        [Key]
        public int Id { get; set; }
        public float Value { get; set; }
        public bool Paid { get; set; }
        public float Quantity { get; set; }
        public DateTime Date { get; set; }
        [ForeignKey("Providers")]
        public int ProviderId { get; set; }
        [ForeignKey("Apartments")]
        public int ApartmentId { get; set; }

        // Navigation properties
        public Providers Provider { get; set; }
        public Apartments Apartment { get; set; }
    }
}
