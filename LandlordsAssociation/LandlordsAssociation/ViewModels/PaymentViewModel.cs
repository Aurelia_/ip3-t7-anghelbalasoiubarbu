﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LandlordsAssociation.ViewModels
{
    public class PaymentViewModel
    {
        public int BillId { get; set; }
        [Range(1, 30, ErrorMessage = "The apartment number must be in the interval [1, 30].")]
        public int ApartmentNumber { get; set; }
        [Range(1, 30, ErrorMessage = "The block number must be greater than 1.")]
        public int BlockNumber { get; set; }
        public string EntranceName { get; set; }
        [Range(0, float.MaxValue, ErrorMessage = "The value must be positive.")]
        public float Value { get; set; }
    }
}
