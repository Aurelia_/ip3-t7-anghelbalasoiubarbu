﻿using LandlordsAssociation.Data;
using LandlordsAssociation.Models;
using LandlordsAssociation.ViewModels;
using System;
using Xunit;

namespace LandlordsAssociation.Tests
{
    public class ProvidersControllerTest
    {
        private LandlordsAssociationDbContext context;

        [Fact]
        public void AddProvider_WithValidData_Succeeds()
        {
            context = FactoryDB.CreateDB();

            bool providerWasAdded = false;
            bool providerContractWasCreated = false;
            var hireProviderVM = new HireProviderViewModel
            {
                Name = "Enlighten your house",
                Address = "Deva, B-dul. M. Viteazul, Str.Zorilor, nr.1",
                ExpirationDate = new DateTime(2021, 5, 10),
                Price = 3000,
                Service = "electricity"
            };
            var provider = new Providers
            {
                Name = hireProviderVM.Name,
                Service = hireProviderVM.Service,
                Address = hireProviderVM.Address
            };
            context.Add(provider);
            var providerContract = new ProviderContracts
            {
                ProviderId = provider.Id,
                Date = DateTime.Now,
                ExpirationDate = hireProviderVM.ExpirationDate,
                Price = hireProviderVM.Price
            };

            context.ProviderContracts.Add(providerContract);
            context.SaveChanges();


            if (context.Providers.Find(provider.Id) != null)
            {
                providerWasAdded = true;
            }
            if (context.ProviderContracts.Find(providerContract.Id) != null)
            {
                providerContractWasCreated = true;
            }
            context.Database.EnsureDeleted();
            Assert.True(providerWasAdded && providerContractWasCreated);
        }

        [Fact]
        public void Delete_ExistingProvider_Succeeds()
        {
            // Arrange
            context = FactoryDB.CreateDB();
            var provider = new Providers { Id = 3, Name = "Heat House", Address = "Deva, B-dul. Decebal, nr.105", Service = "heat" };
            var contract = new ProviderContracts { Id = 3, Date = DateTime.Now, ExpirationDate = new DateTime(2020, 5, 15), Price = 4000, ProviderId = provider.Id };
            context.Providers.Add(provider);
            context.ProviderContracts.Add(contract);
            context.SaveChanges();

            // Act
            var providerToBeDeleted = context.Providers.Find(3);
            context.Providers.Remove(providerToBeDeleted);
            context.SaveChanges();
            var providerContract = context.ProviderContracts.Find(3);
            context.Database.EnsureDeleted();

            // Assert
            Assert.Null(providerContract);
        }

        [Fact]
        public void Update_ExtendProviderContract_Succeeds()
        {
            //Arrange
            context = FactoryDB.CreateDB();
            var provider = new Providers { Id = 4, Name = "Heat House", Address = "Deva, B-dul. Decebal, nr.105", Service = "heat" };
            var contract = new ProviderContracts { Id = 4, Date = DateTime.Now, ExpirationDate = new DateTime(2020, 5, 15), Price = 4000, ProviderId = provider.Id };
            context.Providers.Add(provider);
            context.ProviderContracts.Add(contract);
            context.SaveChanges();
            // Act
            var contractProviderToBeUpdated = context.ProviderContracts.Find(4);
            var oldExpirationDate = contractProviderToBeUpdated.ExpirationDate;
            contractProviderToBeUpdated.ExpirationDate = new DateTime(2022, 10, 10);
            context.ProviderContracts.Update(contractProviderToBeUpdated);
            context.SaveChanges();
            bool contractWasNotUpdated = String.Equals(oldExpirationDate.ToString(), contractProviderToBeUpdated.ExpirationDate.ToString());
            context.Database.EnsureDeleted();

            // Assert
            Assert.False(contractWasNotUpdated);
        }
    }
}
