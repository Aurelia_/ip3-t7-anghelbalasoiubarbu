﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LandlordsAssociation.Migrations
{
    public partial class TypoFix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Service",
                table: "AdditionalBills",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        { 

            migrationBuilder.AlterColumn<int>(
                name: "Service",
                table: "AdditionalBills",
                type: "int",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
