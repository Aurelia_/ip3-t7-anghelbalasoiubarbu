﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LandlordsAssociation.Models
{
    public class Payment
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey("Apartments")]
        public int ApartmentId { get; set; }
        public float Value { get; set; }
        public DateTime Date { get; set; }

        public Apartments Apartment { get; set; }
    }
}
