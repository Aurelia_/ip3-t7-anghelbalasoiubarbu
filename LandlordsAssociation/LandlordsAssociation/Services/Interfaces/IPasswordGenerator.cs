﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LandlordsAssociation.Services.Interfaces
{
    public interface IPasswordGenerator
    {
        public string GeneratePassword(int length);
    }
}
