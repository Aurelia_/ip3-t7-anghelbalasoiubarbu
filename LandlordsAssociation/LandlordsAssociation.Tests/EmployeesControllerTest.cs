﻿using LandlordsAssociation.Data;
using LandlordsAssociation.Models;
using LandlordsAssociation.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace LandlordsAssociation.Tests
{
    [Collection("Serialize")]
    public class EmployeesControllerTest
    {
        private LandlordsAssociationDbContext _context;


        public EmployeesControllerTest()
        {
            var options = new DbContextOptionsBuilder<LandlordsAssociationDbContext>()
                .UseInMemoryDatabase(databaseName: "LandlordsAssociationTestDatabase")
                .Options;
            _context = new LandlordsAssociationDbContext(options);

        }

        [Theory]
        [InlineData("12345678910112", "Secretary", "Administrator")]
        [InlineData("12345678910113", "Accountant", "Administrator")]
        [InlineData("12345678910114", "Block administrator", "User")]
        public void CreatePost_CreateEmployeeWithAccount_Succeeds(string cnp, string job, string role)
        {
            var employee = new Employees
            {
                CNP = cnp,
                Email = "test_employee@la.com",
                Address = "-",
                FirstName = "John",
                LastName = "Smith",
                PhoneNumber = "-",
                Salary = 1000,
                Job = job
            };

            if (employee.Job.Equals("Secretary") || 
                employee.Job.Equals("Accountant") || 
                employee.Job.Equals("Block administrator"))
            {
                PasswordService _passwordService = new PasswordService();
                Users user = new Users
                {
                    Username = employee.Email,
                    Password = _passwordService.GeneratePassword(10)
                };
                if (employee.Job.Equals("Block administrator"))
                {
                    user.Role = "User";
                }
                else
                {
                    user.Role = "Administrator";
                }
                _context.Add(user);
                _context.SaveChanges();
                employee.UserId = user.Id;
            }
            else
            {
                employee.User = null;
                employee.UserId = null;
            }


            _context.Employees.Add(employee);
            _context.SaveChanges();
            var result = _context.Employees.Find(employee.CNP);
            Assert.Equal(employee, result);
            Assert.NotNull(result.UserId);
            var resultUser = _context.Users.Find(result.UserId);
            Assert.Equal(role, resultUser.Role);
        }

        [Theory]
        [InlineData("12345678910115", "Watchman")]
        public void CreatePost_CreateEmployeeWithoutAccount_Succeeds(string cnp, string job)
        {
            var employee = new Employees
            {
                CNP = cnp,
                Email = "test_employee@la.com",
                Address = "-",
                FirstName = "John",
                LastName = "Smith",
                PhoneNumber = "-",
                Salary = 1000,
                Job = job
            };

            if (employee.Job.Equals("Secretary") || 
                employee.Job.Equals("Accountant") || 
                employee.Job.Equals("Block administrator"))
            {
                PasswordService _passwordService = new PasswordService();
                Users user = new Users
                {
                    Username = employee.Email,
                    Password = _passwordService.GeneratePassword(10)
                };
                if (employee.Job.Equals("Block administrator"))
                {
                    user.Role = "User";
                }
                else
                {
                    user.Role = "Administrator";
                }
                _context.Add(user);
                _context.SaveChanges();
                employee.UserId = user.Id;
            }
            else
            {
                employee.User = null;
                employee.UserId = null;
            }


            _context.Employees.Add(employee);
            _context.SaveChanges();
            var result = _context.Employees.Find(employee.CNP);
            Assert.Equal(employee, result);
            Assert.Null(result.UserId);
        }

        [Theory]
        [InlineData("1111111111111", true, "Administrator")]
        [InlineData("1111111111112", true, "User")]
        [InlineData("1111111111119", false, null)]
        public async Task DeletePost_DeleteEmployeeWithAccount_Succeeds(string cnp, bool hasAccount, string accountType)
        {

            // Seed the database
            await Seed();
            var foundEmployee = _context.Employees.Find(cnp);
            if (foundEmployee.UserId != null)
            {
                var foundUser = _context.Users.Find(foundEmployee.UserId);
                Assert.NotNull(foundUser);
                Assert.Equal(accountType, foundUser.Role);
                if (foundUser != null)
                {
                    _context.Users.Remove(foundUser);
                }

            }
            _context.Employees.Remove(foundEmployee);
            _context.SaveChanges();
            if (hasAccount)
            {
                var result = _context.Users.Find(foundEmployee.UserId);
                Assert.Null(result);
            }
            Assert.False(await _context.Employees.ContainsAsync(foundEmployee));

        }

        // this is just for me
        [Fact(Skip = "This is just a test used to test if the database is empty")]

        public async Task Select()
        {
            Assert.Equal(0, await _context.Users.CountAsync());
        }

        [Theory]
        [InlineData("1111111111116", true, "Administrator", "Block administrator", "User")]
        [InlineData("1111111111117", true, "User", "Secretary", "Administrator")]
        [InlineData("1111111111118", false, null, "Block administrator", "User")]
        [InlineData("1111111111115", true, "Administrator", "Watchman", null)]
        [InlineData("1111111111114", false, null, "Secretary", "Administrator")]
        [InlineData("1111111111113", true, "User", "Watchman", null)]
        public async Task EditPost_EditEmployee_Succeeds(string cnp, bool hasAccount, string accountType, string job, string newAccountType)
        {
            await Seed();

            PasswordService _passwordService = new PasswordService();
            var foundEmployee = _context.Employees.Find(cnp);
            var tempEmployee = new Employees
            {
                CNP = cnp,
                Email = "employee@la.com",
                Address = "-",
                FirstName = "John",
                LastName = "Smith",
                PhoneNumber = "-",
                Salary = 1000,
                Job = job
            };

            if (tempEmployee.Email != foundEmployee.Email)
            {
                var foundUser = _context.Users.Find(foundEmployee.UserId);
                if (foundUser != null)
                {
                    // if the employee changes his email and they have an account
                    foundUser.Username = tempEmployee.Email;
                    _context.Update(foundUser);
                    _context.SaveChanges();
                    Assert.Equal(tempEmployee.Email, _context.Users.Find(foundEmployee.UserId).Username);
                }
            }
            // update the employee
            foundEmployee.LastName = tempEmployee.LastName;
            foundEmployee.FirstName = tempEmployee.FirstName;
            foundEmployee.Address = tempEmployee.Address;
            foundEmployee.Email = tempEmployee.Email;
            foundEmployee.Job = tempEmployee.Job;
            foundEmployee.PhoneNumber = tempEmployee.PhoneNumber;
            foundEmployee.Salary = tempEmployee.Salary;

            if (foundEmployee.Job.Equals("Secretary") || 
                foundEmployee.Job.Equals("Accountant") || 
                foundEmployee.Job.Equals("Block administrator"))
            {
                if (foundEmployee.UserId == null)
                {
                    Assert.NotEqual(foundEmployee.UserId == null, hasAccount);
                    // if the new job requires an account
                    Users user = new Users
                    {
                        Username = tempEmployee.Email,
                        Password = _passwordService.GeneratePassword(10)
                    };
                    if (tempEmployee.Job.Equals("Block administrator"))
                    {
                        user.Role = "User";
                    }
                    else
                    {
                        user.Role = "Administrator";
                    }
                    _context.Add(user);
                    _context.SaveChanges();

                    foundEmployee.UserId = user.Id;
                }
                else
                {
                    var foundUser = _context.Users.Find(foundEmployee.UserId);
                    if ((foundEmployee.Job.Equals("Secretary") || 
                        foundEmployee.Job.Equals("Accountant")) && 
                        foundUser.Role.Equals("User"))
                    {
                        foundUser.Role = "Administrator";
                        _context.Update(foundUser);
                        _context.SaveChanges();
                    }
                    if (foundEmployee.Job.Equals("Block administrator") && foundUser.Role.Equals("Administrator"))
                    {
                        foundUser.Role = "User";
                        _context.Update(foundUser);
                        _context.SaveChanges();
                    }
                    Assert.NotEqual(accountType, _context.Users.Find(foundEmployee.UserId).Role);
                    Assert.Equal(newAccountType, _context.Users.Find(foundEmployee.UserId).Role);
                   
                }

            }
            else
            {
                // if the new job does not require a user account
                if (foundEmployee.UserId != null)
                {
                    var redundantUser = _context.Users.Find(foundEmployee.UserId);
                    foundEmployee.UserId = null;
                    foundEmployee.User = null;
                    _context.Users.Remove(redundantUser);
                    _context.SaveChanges();
                    Assert.False(await _context.Users.ContainsAsync(redundantUser));
                }
            }
            // update:
            _context.Update(foundEmployee);
            _context.SaveChanges();
            if (newAccountType != null)
            {
                Assert.Equal(newAccountType, _context.Users.Find(foundEmployee.UserId).Role);
                Assert.True(await _context.Users.ContainsAsync(foundEmployee.User));
            }

            // check if the employee was updated
            Assert.Equal(tempEmployee.Email, _context.Employees.Find(foundEmployee.CNP).Email);
            Assert.Equal(tempEmployee.Salary, _context.Employees.Find(foundEmployee.CNP).Salary);
            Assert.Equal(tempEmployee.FirstName, _context.Employees.Find(foundEmployee.CNP).FirstName);
            Assert.Equal(tempEmployee.LastName, _context.Employees.Find(foundEmployee.CNP).LastName);
            Assert.Equal(tempEmployee.PhoneNumber, _context.Employees.Find(foundEmployee.CNP).PhoneNumber);
        }

        [Fact]
        public async Task Seed()
        {
            if (await _context.Users.CountAsync() != 0 ||
                _context.Employees.Find("1111111111111") != null)
            {
                return;
            }

            _context.Users.AddRange(
                new Users
                {
                    Id = 1,
                    Username = "employee1@la.com",
                    Password = "PASSWORD1234",
                    Role = "Administrator"
                },
                new Users
                {
                    Id = 2,
                    Username = "employee2@la.com",
                    Password = "PASSWORD1234",
                    Role = "User"
                },
                new Users
                {
                    Id = 3,
                    Username = "employee6@la.com",
                    Password = "PASSWORD1234",
                    Role = "Administrator"
                },
                new Users
                {
                    Id = 4,
                    Username = "employee7@la.com",
                    Password = "PASSWORD1234",
                    Role = "User"
                },
                new Users
                {
                    Id = 5,
                    Username = "employee3@la.com",
                    Password = "PASSWORD1234",
                    Role = "Administrator"
                }
            );
            _context.SaveChanges();
            var employees = new[]
            {
                new Employees
                {
                    CNP = "1111111111111",
                    Email = "employee1@la.com",
                    Address = "-",
                    FirstName = "Anna",
                    LastName = "Christof",
                    PhoneNumber = "-",
                    Salary = 2500,
                    Job = "Secretary",
                    UserId = 1
                },
                new Employees
                {
                    CNP = "1111111111112",
                    Email = "employee2@la.com",
                    Address = "-",
                    FirstName = "Emmanuel",
                    LastName = "Johnson",
                    PhoneNumber = "-",
                    Salary = 2000,
                    Job = "Block administrator",
                    UserId = 2
                },
                new Employees
                {
                    CNP = "1111111111113",
                    Email = "employee3@la.com",
                    Address = "-",
                    FirstName = "Christopher",
                    LastName = "Davis",
                    PhoneNumber = "-",
                    Salary = 1500,
                    Job = "Accountant",
                    UserId = 5
                },
                new Employees
                {
                    CNP = "1111111111114",
                    Email = "employee4@la.com",
                    Address = "-",
                    FirstName = "Cornelius",
                    LastName = "Smith",
                    PhoneNumber = "-",
                    Salary = 1500,
                    Job = "Janitor",
                    UserId = null
                },
                new Employees
                {
                    CNP = "1111111111115",
                    Email = "employee5@la.com",
                    Address = "-",
                    FirstName = "Clara",
                    LastName = "Jaxon",
                    PhoneNumber = "-",
                    Salary = 1500,
                    Job = "Watchman",
                    UserId = null
                },
                new Employees
                {
                    CNP = "1111111111116",
                    Email = "employee6@la.com",
                    Address = "-",
                    FirstName = "John",
                    LastName = "Smith",
                    PhoneNumber = "-",
                    Salary = 2500,
                    Job = "Secretary",
                    UserId = 3
                },
                new Employees
                {
                    CNP = "1111111111117",
                    Email = "employee7@la.com",
                    Address = "-",
                    FirstName = "John",
                    LastName = "Smith",
                    PhoneNumber = "-",
                    Salary = 2000,
                    Job = "Block administrator",
                    UserId = 4
                },
                new Employees
                {
                    CNP = "1111111111118",
                    Email = "employee8@la.com",
                    Address = "-",
                    FirstName = "Helen",
                    LastName = "Evans",
                    PhoneNumber = "-",
                    Salary = 1700,
                    Job = "Electrician",
                    UserId = null
                }
                ,
                new Employees
                {
                    CNP = "1111111111119",
                    Email = "employee9@la.com",
                    Address = "-",
                    FirstName = "Jonathan",
                    LastName = "Davis",
                    PhoneNumber = "-",
                    Salary = 1700,
                    Job = "Electrician",
                    UserId = null
                }
            };


            _context.Employees.AddRange(employees);
            _context.SaveChanges();
        }
    }
}
