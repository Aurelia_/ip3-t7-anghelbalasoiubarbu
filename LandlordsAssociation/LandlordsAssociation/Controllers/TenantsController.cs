﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using LandlordsAssociation.Data;
using LandlordsAssociation.Models;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace LandlordsAssociation.Controllers
{
    public class TenantsController : Controller
    {
        private readonly LandlordsAssociationDbContext _context;

        public TenantsController(LandlordsAssociationDbContext context)
        {
            _context = context;
        }

        // GET: Tenants/Create
        public IActionResult Create()
        {
            var sessionContent = HttpContext.Session.GetString("loggedInUser");
            // check if a user is logged
            if (sessionContent == null)
            {
                return RedirectToAction("Index", "Home");
            }
            // get the details of the logged in user
            var user = JsonConvert.DeserializeObject<Users>(sessionContent);
            int id = user.Id;
            var foundUser = _context.Users.Where(u => u.Id == id).Single();

            if (foundUser.Role.Equals("Administrator"))
            {
                var blocks = _context.BlockOfFlats.ToList().Distinct();
                ViewData["Blocks"] = new SelectList(blocks, "Id", "Id");
                Tenants model = new Tenants();
                return View(model);
            }
            else
            {
                return RedirectToAction("Logout", "Users");
            }
        }

        // POST: Tenants/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CNP,FirstName,LastName,PhoneNumber")] Tenants tenants)
        {
            if (ModelState.IsValid)
            {
                // search the tenant and if he does not exist, then add him 
                Tenants tenant = _context.Tenants.Where(t => t.CNP.Equals(tenants.CNP)).FirstOrDefault();
                if (tenant == null)
                {
                    _context.Tenants.Add(tenants);
                    await _context.SaveChangesAsync();
                }
                
                return RedirectToAction("AssignApartmentToTenant", "Apartments");
            }
            var blocks = _context.BlockOfFlats.ToList().Distinct();
            ViewData["Blocks"] = new SelectList(blocks, "Id", "Id");
            ViewData["Message"] = "The data is not valid. Try again...";
            return View(tenants);
        }


        // GET: Tenants/Delete/5
        public IActionResult Delete()
        {
            var sessionContent = HttpContext.Session.GetString("loggedInUser");
            // check if a user is logged
            if (sessionContent == null)
            {
                return RedirectToAction("Index", "Home");
            }
            // get the details of the logged in user
            var user = JsonConvert.DeserializeObject<Users>(sessionContent);
            int id = user.Id;
            var foundUser = _context.Users.Where(u => u.Id == id).Single();

            if (foundUser.Role.Equals("Administrator"))
            {
                ViewData["CNP"] = new SelectList(_context.Tenants, "CNP", "CNP");
                return View();
            }
            else
            {
                return RedirectToAction("Logout", "Users");
            }
        }

        // POST: Tenants/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string CNP)
        {
            var tenant = _context.Tenants.Where(t => t.CNP.Equals(CNP)).FirstOrDefault();
            // get the apartment rent and clear it
            var freeApartments = (from a in _context.Apartments
                                 join c in _context.TenantContracts on a.ContractId equals c.Id
                                 where c.TenantCNP.Equals(tenant.CNP)
                                 select a).ToList();
            foreach(var a in freeApartments)
            {
                a.NumberPersons = 0;
                _context.Apartments.Update(a);
            }
                                 
            var rents = (from rent in _context.Rent
                         join apartment in _context.Apartments on rent.ApartmentId equals apartment.Id
                         join contract in _context.TenantContracts on apartment.ContractId equals contract.Id
                         where contract.TenantCNP.Equals(CNP)
                         select rent).ToList();
            foreach(var rent in rents)
            {
                _context.Rent.Remove(rent);
            }
            // delete the water consumption:
            var waterConsumption = (from water in _context.WaterConsumption
                                    join apartment in _context.Apartments on water.ApartmentId equals apartment.Id
                                    join contract in _context.TenantContracts on apartment.ContractId equals contract.Id
                                    where contract.TenantCNP.Equals(CNP)
                                    select water).ToList();
            foreach (var element in waterConsumption)
            {
                _context.WaterConsumption.Remove(element);
            }
            // delete the provider bills
            var bills = (from bill in _context.ProviderBills
                         join apartment in _context.Apartments on bill.ApartmentId equals apartment.Id
                         join contract in _context.TenantContracts on apartment.ContractId equals contract.Id
                         where contract.TenantCNP.Equals(CNP)
                         select bill).ToList();
            foreach (var bill in bills)
            {
                _context.ProviderBills.Remove(bill);
            }
            _context.Tenants.Remove(tenant);
            await _context.SaveChangesAsync();
            return RedirectToAction("SeeIncomeAndCosts", "Bills");
        }
    }
}
