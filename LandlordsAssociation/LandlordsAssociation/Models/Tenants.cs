﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LandlordsAssociation.Models
{
    public class Tenants
    {
        [Key]
        [RegularExpression(@"(1|2)[0-9]{12}", ErrorMessage = "The CNP must start with 1 or 2.")]
        [StringLength(13, MinimumLength = 13, ErrorMessage = "The CNP must have 13 digits.")]
        public string CNP { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }

        // Navigation properties
        public ICollection<TenantContracts> Contracts { get; set; }
    }
}
