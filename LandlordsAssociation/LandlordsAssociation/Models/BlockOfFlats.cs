﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LandlordsAssociation.Models
{
    public class BlockOfFlats
    {
        [Key]
        public int Id { get; set; }
        public string Street { get; set; }
        public string City { get; set; }

        // Navigation properties
        public ICollection<Apartments> Apartments { get; set; }
    }
}
