﻿using LandlordsAssociation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LandlordsAssociation.ViewModels
{
    public class ApartmentDebtViewModel
    {
        public Apartments Apartment { get; set; }
        public double Debt { get; set; }
        public double UnpaidRent { get; set; }
        public double WaterBill { get; set; }
    }
}
