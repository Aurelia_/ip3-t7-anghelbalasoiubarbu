﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LandlordsAssociation.Models
{
    public class Employees
    {
        [Key]
        [RegularExpression(@"(1|2)[0-9]{12}", ErrorMessage = "The CNP must start with 1 or 2.")]
        [StringLength(13, MinimumLength = 13, ErrorMessage = "The CNP must have 13 digits.")]
        public string CNP { get; set; }
        public string FirstName { get; set; }
        public string LastName{ get; set; }
        [Range(100,10000)]
        public float Salary { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Job { get; set; }

        // Navigation properties
        [ForeignKey("Users")]
        public int? UserId { get; set; }
        public Users User { get; set; }
    }
}
