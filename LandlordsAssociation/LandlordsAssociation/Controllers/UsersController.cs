﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using LandlordsAssociation.Data;
using LandlordsAssociation.Models;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace LandlordsAssociation.Controllers
{
    public class UsersController : Controller
    {
        private readonly LandlordsAssociationDbContext _context;

        public UsersController(LandlordsAssociationDbContext context)
        {
            _context = context;
        }

        // GET: Users/Login
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(Users user)
        {
            var foundUser = _context.Users.Where(x => x.Username == user.Username).Where(x => x.Password == user.Password).SingleOrDefault();

            if (foundUser != null)
            {
                HttpContext.Session.SetString("loggedInUser", JsonConvert.SerializeObject(foundUser));
                if (foundUser.Role.Equals("Administrator"))
                {
                    return RedirectToAction("SeeIncomeAndCosts", "Bills");
                }
                else
                {
                    return RedirectToAction("ShowDebtList", "Bills");
                }
                
            }
            else
            {
                ViewData["Message"] = "Invalid credentials...";
            }

            return View();
        }

        public ActionResult Logout()
        {
            HttpContext.Session.Clear();
            return RedirectToAction("Index", "Home");
        }

        // GET: Users/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            var loggedUser = HttpContext.Session.GetString("loggedInUser");
            if(loggedUser != null)
            {
                var currentUser = JsonConvert.DeserializeObject<Users>(loggedUser);

                if (id == null)
                {
                    id = currentUser.Id;
                }

                var users = await _context.Users
                    .FirstOrDefaultAsync(m => m.Id == id);
                if (users == null)
                {
                    return NotFound();
                }
                ViewData["CurrentUser"] = currentUser;
                return View(users);

            }
            else
            {
                return RedirectToAction("Login", "Users");
            }


        }

        // GET: Users/Edit/5
        public async Task<IActionResult> Edit()
        {
            var sessionContent = HttpContext.Session.GetString("loggedInUser");
            // check if a user is logged
            if (sessionContent == null)
            {
                return RedirectToAction("Index", "Home");
            }
            //get the details of the logged in user
            var user = JsonConvert.DeserializeObject<Users>(sessionContent);
            int id = user.Id;
            var foundUser = _context.Users.Where(u => u.Id == id).Single();

            return View(foundUser);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Username,Password,Role")] Users users)
        {
            if (id != users.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var foundUser = _context.Users.Where(u => u.Id == users.Id).SingleOrDefault();
                    if (users.Username != null && users.Username != foundUser.Username)
                    {
                        foundUser.Username = users.Username;
                    }
                        
                    if(users.Password != null && users.Password != foundUser.Password)
                    {
                        foundUser.Password = users.Password;
                    }
                    
                    var foundEmployee = _context.Employees.Where(e => e.UserId == users.Id).SingleOrDefault();
                    foundEmployee.Email = users.Username;
                   
                    _context.Update(foundEmployee);
                    _context.Update(foundUser);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!_context.Users.Any(e => e.Id == users.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Edit));
            }
            return View(users);
        }
    }
}
