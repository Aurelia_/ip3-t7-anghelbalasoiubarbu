﻿using LandlordsAssociation.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LandlordsAssociation.ViewModels
{
    public class DebtForApartmentViewModel
    {
        [Required]
        [Range(1, 100, ErrorMessage = "The id of the block must be in the interval [1, 100]")]
        public int BlockNumber { get; set; }
        [Required]
        public string Entrance { get; set; }
        [Required]
        [Range(1, 30, ErrorMessage = "The number of the apartment must be in the interval [1, 30]")]
        public int Number { get; set; }
    }
}
