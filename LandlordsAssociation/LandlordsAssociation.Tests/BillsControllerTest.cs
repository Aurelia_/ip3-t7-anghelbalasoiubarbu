﻿using LandlordsAssociation.Data;
using LandlordsAssociation.Models;
using LandlordsAssociation.ViewModels;
using System;
using System.Linq;
using Xunit;

namespace LandlordsAssociation.Tests
{
    public class BillsControllerTest
    {
        [Fact]
        public void AddProviderBill_BillData_Succeeds()
        {
            var context = FactoryDB.CreateDB();
            Seed(context);

            int billId = 1;
            int apartmentNumber = 10;
            int blockNumber = 11;
            string providerName = "Heat House";
            string tenantCNP = "2710107865431";
            float value = 200;

            // Act
            var block = context.BlockOfFlats.Where(b => b.Id == blockNumber).FirstOrDefault();
            var contract = context.TenantContracts.Where(c => c.TenantCNP == tenantCNP).FirstOrDefault();
            var service = (from c in context.ProviderContracts
                           join provider in context.Providers on c.ProviderId equals provider.Id
                           where provider.Name.Equals(providerName)
                           select provider.Service).SingleOrDefault();
            if (block != null && contract != null && service != "Water")
            {
                var apartment = context.Apartments.Where(a => a.BlockId == block.Id && a.Number == apartmentNumber 
                    && a.ContractId == contract.Id).FirstOrDefault();
                if (apartment != null)
                {
                    var provider = context.Providers.Where(p => p.Name == providerName).FirstOrDefault();
                    if (provider != null)
                    {
                        var providerContract = context.ProviderContracts.Where(p => p.Id == provider.Id).FirstOrDefault();
                        if (providerContract != null)
                        {
                            var quantity = value / providerContract.Price;
                            ProviderBills bill = new ProviderBills
                            {
                                Id = billId,
                                Value = value,
                                Paid = false,
                                Quantity = quantity,
                                Date = DateTime.Today,
                                ProviderId = provider.Id,
                                ApartmentId = apartment.Id,
                            };
                            context.ProviderBills.Add(bill);
                            context.SaveChanges();
                        }
                    }
                    // Assert
                    Assert.NotNull(context.ProviderBills.Find(billId));
                    context.Database.EnsureDeleted();
                }
            }
        }

        private void Seed(LandlordsAssociationDbContext context)
        {
            context.BlockOfFlats.Add(new BlockOfFlats { Id = 11, City = "Craiova", Street = "Mihai Viteazul" });
            var apartment = new Apartments { Id = 45, BlockId = 11, ContractId = null, Entrance = "A", Floor = 2, 
                    Number = 10, NumberRooms = 4, NumberPersons = 0 };
            context.Apartments.Add(apartment);
            context.Tenants.Add(new Tenants { CNP = "2710107865431", FirstName = "Lisa", LastName = "Derek", 
                PhoneNumber = "0711111111" });
            context.TenantContracts.Add(new TenantContracts { Id = 2, Apartment = apartment, Date = new DateTime(2019 - 10 - 10), 
                TenantCNP = "2710107865431", Type = "Rent", Value = 1500 });
            context.Providers.Add(new Providers { Id = 5, Address = "Craiova, B-dul. M. Viteazul, Str.Zorilor, nr.1", 
                Name = "Enlighten your house", Service = "electricity"});
            context.ProviderContracts.Add(new ProviderContracts { Id = 10, Date = new DateTime(2020, 05, 20), 
                ExpirationDate = new DateTime(2022, 05, 20), ProviderId = 5, Price = 2000 });
            context.SaveChanges();
        }
    }
}
